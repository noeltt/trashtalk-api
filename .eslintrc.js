module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2021: true
    },
    extends: [
        'airbnb-base'
    ],
    parserOptions: {
        ecmaVersion: 13
    },
    rules: {
        'react/prop-types': 'off',
        'react/display-name': 'off',
        'no-undef': 'off',
        'prefer-destructuring': 'off',
        'array-callback-return': 'off',
        'no-floating-decimal': 'off',
        'space-before-function-paren': 'off',
        indent: ['error', 4],
        semi: [2, 'never'],
        'comma-dangle': ['error', 'never'],
        'no-console': 'off',
        'max-len': 'off',
        'function-paren-newline': 'off',
        'object-curly-newline': 'off',
        'no-param-reassign': 'off',
        'multiline-ternary': 'off',
        'brace-style': [2, 'stroustrup'],
        radix: ['error', 'as-needed'],
        camelcase: 'off' // ['error', { properties: 'never' }]
    }
}
