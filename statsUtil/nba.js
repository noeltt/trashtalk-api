// scheduled jobs
const axios = require('axios').default
const config = require('../config.json')[global.env]

const statsDal = require('../DAL/statsDAL')

const { getYearMonthDayString } = require('../utils/mainUtils')

const chatRoute = `${config.chatServerUrl}/updategamestats`
// const chatRoute = 'http://localhost:3000'

const SportsAPIBaseUrl = 'https://v1.basketball.api-sports.io'
const SportsAPIOptionHeaders = {
    headers: {
        'Content-Type': 'application/json',
        'x-apisports-key': 'a5ce817324cf77015bda43300d19d96b',
    },
}

const formatGames = (games) => {
    const gameDetails = []

    games.forEach((game) => {
        if (game.status.long !== 'Not Started') {
            // console.log({ game })
        }
        const start_time = new Date(game.date)
        // let timeZoneOffset = start_time.getTimezoneOffset()
        // console.log({timeZoneOffset})
        const timeZoneOffsetMilli = start_time.getTimezoneOffset() * 60 * 1000
        const start_time_utc = new Date(start_time.getTime() + timeZoneOffsetMilli)

        const nba_api_game_id = parseInt(game.id)
        const home_team_nba_api_id = parseInt(game.teams.home.id)
        const away_team_nba_api_id = parseInt(game.teams.away.id)
        let status =
            game.status.long && game.status.long.length > 0
                ? game.status.long.toLowerCase().trim()
                : 'finished'
        const clock = game.status.timer || '00:00'
        // TODO: update
        const quarter = 0
        const home_score = parseInt(game.scores.home.total) || 0
        const away_score = parseInt(game.scores.away.total) || 0

        status = status === 'game finished' ? 'finished' : status

        const gameDetail = [
            nba_api_game_id,
            start_time_utc,
            home_team_nba_api_id,
            away_team_nba_api_id,
            status,
            clock,
            quarter,
            home_score,
            away_score,
        ]

        gameDetails.push(gameDetail)
    })

    return gameDetails
}

// SHOULD ALMOST NEVER BE CALLED
const seedTeamBasics = async (conf) => {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'x-rapidapi-host': 'api-nba-v1.p.rapidapi.com',
            'x-rapidapi-key': 'a9052a3575msh2f7fc6294200e81p1c76d7jsnb793d0324bb9',
        },
    }

    const route = `https://api-nba-v1.p.rapidapi.com/teams/confName/${conf}`
    const res = await axios(route, options)

    if (res.status !== 200) {
        console.error('SOMETHING WENT WRONG WITH THE SEEDING CALL')
        return
    }

    const { teams } = res.data.api

    const teamSeedData = []

    teams.forEach((team) => {
        const nba_api_team_id = parseInt(team.teamId)
        const logo_url = team.logo
        const full_name = team.fullName
        const short_name = team.shortName
        const nickname = team.nickname
        const conference = conf

        const teamData = [nba_api_team_id, logo_url, full_name, short_name, nickname, conference]

        // to avoid the all star teams in the data
        if (logo_url && logo_url.length > 0) teamSeedData.push(teamData)
    })

    await statsDal.seedTeamBasics(teamSeedData)
}

const seedTeamDetails = async () => {
    const options = {
        method: 'GET',
        // these need to eventually be configged
        ...SportsAPIOptionHeaders,
    }

    const route = `${SportsAPIBaseUrl}/standings?league=12&season=2022-2023`
    const res = await axios(route, options)

    if (res.status !== 200) {
        console.error('SOMETHING WENT WRONG WITH THE SEEDING DETAILS CALL')
        return
    }

    const { response } = res.data

    if (response.length > 0) {
        const standings = response[0]

        const teamDetails = []

        standings.forEach((data) => {
            const nba_api_team_id = data.team.id
            const win = data.games.win.total ?? 0
            const loss = data.games.lose.total ?? 0
            const win_percentage = data.games.win.percentage ?? 0
            const loss_percentage = data.games.lose.percentage ?? 0
            const last_ten_win = 0
            const last_ten_loss = 0
            const conference_rank = data.position ?? 100
            const conference_win = 0
            const conference_loss = 0
            const home_win = 0
            const home_loss = 0
            const away_win = 0
            const away_loss = 0
            const win_streak = 0
            const games_behind = 0

            const teamDetail = [
                nba_api_team_id,
                win,
                loss,
                win_percentage,
                loss_percentage,
                last_ten_win,
                last_ten_loss,
                conference_rank,
                conference_win,
                conference_loss,
                home_win,
                home_loss,
                away_win,
                away_loss,
                win_streak,
                games_behind,
            ]

            if (conference_rank > 0) teamDetails.push(teamDetail)
        })

        await statsDal.updateTeamDetails(teamDetails)
    }
}

const seedUpcomingGames = async (dateString) => {
    const options = {
        method: 'GET',
        // these need to eventually be configged
        ...SportsAPIOptionHeaders,
    }

    const route = `${SportsAPIBaseUrl}/games?league=12&season=2022-2023&date=${dateString}`
    const res = await axios(route, options)
    const { response: games, results } = res.data

    if (results > 0) {
        const gameDetails = formatGames(games)

        if (gameDetails.length > 0) {
            await statsDal.upsertNbaGames(gameDetails)
        }
    }
}

exports.seedJobNBA = async (ctx) => {
    await seedTeamBasics('east')
    await seedTeamBasics('west')

    await seedTeamDetails('east')
    await seedTeamDetails('west')

    ctx.body = {
        success: true,
    }
}

exports.updateNBATeamDetails = async () => {
    await seedTeamDetails()
}

exports.getUpcomingGameDetails = async () => {
    const oneDayDiff = 24 * 60 * 60 * 1000
    const today = new Date()
    const tomorrow = new Date(today.getTime() + oneDayDiff)
    const afterTomorrow = new Date(today.getTime() + 1 * oneDayDiff)
    const doubleAfterTomorrow = new Date(today.getTime() + 2 * oneDayDiff)
    // const tripleAfterTomorrow = new Date(today.getTime() + 3 * oneDayDiff)

    const todayString = getYearMonthDayString(today)
    const tomorrowString = getYearMonthDayString(tomorrow)
    const afterTomorrowString = getYearMonthDayString(afterTomorrow)
    const doubleAfterTomorrowString = getYearMonthDayString(doubleAfterTomorrow)
    // const tripleAfterTomorrowString = getYearMonthDayString(tripleAfterTomorrow)

    await seedUpcomingGames(todayString)
    await seedUpcomingGames(tomorrowString)
    await seedUpcomingGames(afterTomorrowString)
    await seedUpcomingGames(doubleAfterTomorrowString)
    // await seedUpcomingGames(tripleAfterTomorrowString)

    // ctx.body = { success: true }
    console.log('NBA GAMES UPDATED')
}

// exports.updateLiveGames = async () => {
//     const options = {
//         method: 'GET',
//         // these need to eventually be configged
//         headers: {
//             'Content-Type': 'application/json',
//             'x-rapidapi-host': 'api-nba-v1.p.rapidapi.com',
//             'x-rapidapi-key': 'a9052a3575msh2f7fc6294200e81p1c76d7jsnb793d0324bb9',
//         },
//     }

//     const route = 'https://api-nba-v1.p.rapidapi.com/games/live/'
//     const res = await axios(route, options)

//     const { games } = res.data.api

//     const gameDetails = formatGames(games)

//     if (gameDetails.length > 0) await statsDal.upsertNbaGames(gameDetails)
// }

exports.emitLiveGameData = async () => {
    const [liveStatsData] = await statsDal.getLiveGameStats()
    console.log('emitting live game data!')
    console.log('game length', liveStatsData.length)

    for (let i = 0; i < liveStatsData.length; i += 1) {
        const data = {
            gameId: liveStatsData[i].id,
            timeText: liveStatsData[i].clock,
            quarter: liveStatsData[i].quarter,
            homeScore: liveStatsData[i].home_score,
            awayScore: liveStatsData[i].away_score,
            participantCount: liveStatsData[i].chat_participant_count,
        }
        const chatServerOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            data,
        }

        // really need to clean this up
        axios(chatRoute, chatServerOptions)
    }
}

exports.testEmitLiveGameData = async (ctx) => {
    const { gameId, timeText, homeScore, awayScore, quarter } = ctx.request.body

    const data = { gameId, timeText, homeScore, awayScore, quarter }

    const chatServerOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        data,
    }

    console.log('test game data sent')
    await axios(chatRoute, chatServerOptions)
}
