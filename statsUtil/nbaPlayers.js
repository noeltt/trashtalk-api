/* eslint-disable */
const axios = require('axios').default
const statsDAL = require('../DAL/statsDAL')
const { updatePlayerStats } = require('../utils/scraper')

exports.loadPlayersFromAPI = async (ctx) => {
    const route = 'https://nba-player-individual-stats.p.rapidapi.com/players'
    const options = {
        method: 'GET',
        // url: 'https://nba-player-individual-stats.p.rapidapi.com/players',
        headers: {
            'x-rapidapi-host': 'nba-player-individual-stats.p.rapidapi.com',
            'x-rapidapi-key': 'a9052a3575msh2f7fc6294200e81p1c76d7jsnb793d0324bb9',
        },
    }

    const { data } = await axios(route, options)

    const dataLength = data.length

    console.log({ dataLength })
    let counter = 0
    let players = []
    for (let i = 0; i < dataLength; i++) {
        const playerData = data[i]

        const playerWeight = playerData.weight ?? ''

        const player = [
            playerData.firstName,
            playerData.lastName,
            (playerData.firstName + ' ' + playerData.lastName).toLowerCase().trim(),
            playerData.team || '',
            playerData.position,
            playerData.height,
            playerWeight.trim(),
            playerData.jerseyNumber,
            parseInt(playerData.age) || 0,
            playerData.headShotUrl,
        ]

        players.push(player)

        counter++

        let counterCheck = counter % 100

        if (counterCheck === 0 || i === dataLength - 1) {
            console.log('player seeded')
            await statsDAL.seedNBAPlayers(players)
            players = []
        }
    }

    // match players to correct teams in db
    await statsDAL.nbaTeamPlayerAllocate()

    // get stats from
    await updatePlayerStats()

    ctx.body = {}
}
