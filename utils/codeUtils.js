const nodemailer = require('nodemailer')
const { generateCode } = require('./mainUtils')

const codesDAL = require('../DAL/codesDAL')
const usersDAL = require('../DAL/usersDAL')

const sendRegistrationEmail = (email, code) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'help@trashtalksportsapp.com',
            pass: 'thisisnotreal123!',
        },
    })

    const bodyText = `
    Welcome To TrashTalk Beta!
    Your registration verification code is:
        ${code}
    This code will expire in one hour. 
    `

    const mailOptions = {
        from: 'welcome',
        to: email,
        subject: 'Welcome to TrashTalk',
        text: bodyText,
    }
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error)
        } else {
            console.log('Email sent')
        }
    })
}

exports.sendConfirmEmail = async () => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'help@trashtalksportsapp.com',
            pass: 'thisisnotreal123!',
        },
    })

    const mailOptions = {
        from: 'Noel',
        to: 'noeleom@gmail.com',
        subject: 'Nodemailer still working',
    }

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error)
        } else {
            console.log('Email sent')
        }
    })
}

exports.sendRegistrationCode = async (userId, email) => {
    let isNewCode = false
    let newCode = ''
    while (!isNewCode) {
        newCode = generateCode(10)
        // must be a better way to write this
        // something going on here
        let codeExists = await codesDAL.checkRegistrationCode(newCode)
        isNewCode = !codeExists
    }

    codesDAL.addRegistrationCode(userId, newCode)
    sendRegistrationEmail(email, newCode)
}

exports.verifyRegistrationCode = async (code) => {
    const ret = await codesDAL.verifyRegistrationCode(code)

    if (ret === undefined) return false

    const { user_id: userId } = ret
    usersDAL.verifyUser(userId)
    codesDAL.deleteRegistrationCode(userId)

    return true
}
