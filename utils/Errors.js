const errorTypes = {
    customError: (message) => ({ status: 500, message }),
    authError: { status: 500, message: 'Authentication Error!' },
    paramInputError: { status: 500, message: 'Param Input Error' },
    paramInvalidError: { status: 500, message: 'Param Invalid Error' }
}

exports.baseError = (message) => errorTypes.customError(message)
exports.authError = errorTypes.authError
exports.paramInputError = errorTypes.paramInputError
exports.paramInvalidError = errorTypes.paramInvalidError
