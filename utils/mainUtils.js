/* eslint-disable */
exports.defaultPostImageUrl = 'https://trashtalk-post-images.s3.us-east-2.amazonaws.com/image2.png'
exports.defaultAvatarUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/defaultprofile.png'
exports.defaultTeamBigImage = 'https://trashtalk-post-images.s3.us-east-2.amazonaws.com/image7.png'

exports.getMinDiffFromNow = (createdDate) => {
    const thisMoment = new Date()
    const postCreated = new Date(createdDate)

    const tzOffsetMin = thisMoment.getTimezoneOffset()

    return Math.floor((thisMoment - postCreated) / 1000 / 60) + tzOffsetMin
}

exports.getYearMonthDayString = (dateTime) => {
    const yearString = String(dateTime.getFullYear())
    let monthString = String(dateTime.getMonth() + 1)
    let dateString = String(dateTime.getDate())

    monthString = monthString.length === 1 ? `0${monthString}` : monthString
    dateString = dateString.length === 1 ? `0${dateString}` : dateString

    return `${yearString}-${monthString}-${dateString}`
}

exports.isEmailValid = (email) =>
    String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )

exports.generateCode = (length) => Math.random().toString(20).slice(2, length).toLowerCase()
