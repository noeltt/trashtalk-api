/* eslint-disable */
const AWS = require('aws-sdk')
const AWSConfig = require('../config.json')[global.env]['AWS']
const { accessKey, secretKey } = AWSConfig
const usersDAL = require('../DAL/usersDAL')

const s3 = new AWS.S3({
    accessKeyId: accessKey,
    secretAccessKey: secretKey,
    region: 'us-east-2',
})

exports.uploadUserProfile = async (userId, userUid, imageBase64) => {
    const base64Data = new Buffer.from(
        imageBase64.replace(/^data:image\/\w+;base64,/, ''),
        'base64'
    )
    const objectParams = {
        // should have dynamic bucket name? or config
        Bucket: 'trashtalk-public',
        Key: `profile_images/user-${userUid}.png`,
        Body: base64Data,
        ContentEncoding: 'base64',
        ContentType: 'image/png',
        ACL: 'public-read',
    }

    try {
        const { Location } = await s3.upload(objectParams).promise()
        await usersDAL.updateUserProfileURL(userId, Location)
    } catch (err) {
        throw { message: 'AWS Image Upload fail' }
    }

    return true
}

exports.uploadUserBaseImage = async (userId, imageBase64) => {
    const base64Data = new Buffer.from(
        imageBase64.replace(/^data:image\/\w+;base64,/, ''),
        'base64'
    )

    // TODO: base on env
    const objectParams = {
        Bucket: 'trashtalk-public',
        Key: `dev-post-images/${userId}-${String(new Date().getTime())}.png`,
        Body: base64Data,
        ContentEncoding: 'base64',
        ContentType: 'image/png',
        ACL: 'public-read',
    }

    try {
        const { Location } = await s3.upload(objectParams).promise()
        return Location
    } catch (err) {
        throw { message: 'AWS Image Upload fail' }
    }
}
