/* eslint-disable */
const { JSDOM } = require('jsdom')
const axios = require('axios').default

const statsDAL = require('../DAL/statsDAL')

const statsUrl = 'https://www.nbastuffer.com/2022-2023-nba-player-stats/'

exports.updatePlayerStats = async () => {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const { data } = await axios(statsUrl, options)

    const statsDom = new JSDOM(data)

    let tableElement = statsDom.window.document.getElementsByClassName('row-hover')[0]
    const playerRows = tableElement.rows

    // can be optimized, but is only run once a day
    for (let i = 0; i < playerRows.length; i++) {
        const playerRow = playerRows.item(i)

        const fullName = playerRow.cells[1].innerHTML.toLowerCase().trim()
        const positionShort = playerRow.cells[3].innerHTML.trim()
        const gamesPlayed = parseFloat(playerRow.cells[5].innerHTML.trim()) || 0
        const minutesPerGame = parseFloat(playerRow.cells[6].innerHTML.trim()) || 0
        const usagePercent = parseFloat(playerRow.cells[7].innerHTML.trim()) || 0
        const turnoverPercent = parseFloat(playerRow.cells[8].innerHTML.trim()) || 0
        const freethrowAttempts = parseFloat(playerRow.cells[9].innerHTML.trim()) || 0
        const freethrowPercent = parseFloat(playerRow.cells[10].innerHTML.trim()) || 0
        const twopointAttempts = parseFloat(playerRow.cells[11].innerHTML.trim()) || 0
        const twopointPercent = parseFloat(playerRow.cells[12].innerHTML.trim()) || 0
        const threepointAttempts = parseFloat(playerRow.cells[13].innerHTML.trim()) || 0
        const threepointPercent = parseFloat(playerRow.cells[14].innerHTML.trim()) || 0
        const effectiveFieldGoalPercent = parseFloat(playerRow.cells[15].innerHTML.trim()) || 0
        const trueShootingPercent = parseFloat(playerRow.cells[16].innerHTML.trim()) || 0
        const pointsPerGame = parseFloat(playerRow.cells[17].innerHTML.trim()) || 0
        const reboundsPerGame = parseFloat(playerRow.cells[18].innerHTML.trim()) || 0
        const assistsPerGame = parseFloat(playerRow.cells[19].innerHTML.trim()) || 0
        const stealsPerGame = parseFloat(playerRow.cells[20].innerHTML.trim()) || 0
        const blocksPerGame = parseFloat(playerRow.cells[21].innerHTML.trim()) || 0

        //these seem to get fuzzy
        const totalReboundPercent = parseFloat(playerRow.cells[21].innerHTML.trim()) || 0
        const assistsPercent = parseFloat(playerRow.cells[22].innerHTML.trim()) || 0
        const turnoverPerGame = parseFloat(playerRow.cells[25].innerHTML.trim()) || 0

        const versatilityIndex = parseFloat(playerRow.cells[26].innerHTML.trim()) || 0
        const offensiveRating = parseFloat(playerRow.cells[27].innerHTML.trim()) || 0
        const defensiveRating = parseFloat(playerRow.cells[28].innerHTML.trim()) || 0

        // seems redundant but less of a headache to edit and read
        const player = [
            positionShort,
            gamesPlayed,
            minutesPerGame,
            usagePercent,
            turnoverPercent,
            freethrowAttempts,
            freethrowPercent,
            twopointAttempts,
            twopointPercent,
            threepointAttempts,
            threepointPercent,
            effectiveFieldGoalPercent,
            trueShootingPercent,
            pointsPerGame,
            reboundsPerGame,
            totalReboundPercent,
            assistsPerGame,
            assistsPercent,
            stealsPerGame,
            blocksPerGame,
            turnoverPerGame,
            versatilityIndex,
            offensiveRating,
            defensiveRating,
            //params
            fullName,
        ]

        // if (fullName === 'stephen curry') {
        //     console.log({ gamesPlayed, pointsPerGame })
        // }

        await statsDAL.updatePlayerData(player)
        //console.log({player})
    }
    console.log('player scrape success')
}
