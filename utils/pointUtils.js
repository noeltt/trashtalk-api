const postsDAL = require('../DAL/postsDAL')

// TODO: points to relevant game and player and team
exports.addPointsToPost = async ({ postId, points, gameId, playerId, teamId }) => {
    await postsDAL.addPointsToPost(postId, points)
}
// TODO: points to relevant game and player and team
exports.subtractPointsFromPost = async ({ postId, points, gameId, playerId, teamId }) => {
    await postsDAL.subtractPointsFromPost(postId, points)
}
