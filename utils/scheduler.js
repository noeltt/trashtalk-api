const scheduler = require('node-schedule')
const seeder = require('../statsUtil/nba')
const scraper = require('./scraper')
const codeUtils = require('./codeUtils')

// set to run every minute
module.exports = scheduler.scheduleJob('1 * * * * *', async () => {
    const minuteCounter = new Date().getMinutes()
    const hourCounter = new Date().getHours()
    console.log({ minuteCounter, hourCounter })

    if (
        global.env === 'production' &&
        minuteCounter === 0 &&
        (hourCounter >= 16 || hourCounter < 6)
    ) {
        await codeUtils.sendConfirmEmail()
    }

    if (minuteCounter % 10 === 0) {
        console.log('updating team details')
        seeder.updateNBATeamDetails()
        scraper.updatePlayerStats()
    }
    // needs to not be run every minute..?
    // console.log('updating and emitting games')
    seeder.getUpcomingGameDetails()
    seeder.emitLiveGameData()
})
