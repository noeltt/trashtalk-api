/* eslint-disable */
const jwt = require('jsonwebtoken')
const usersDAL = require('../DAL/usersDAL')

const { authError } = require('../utils/Errors')
const { tokenKey, loginTokenKey } = require('../config.json')[global.env]
// const { baseError, authError } = require('../utils/Errors')

// TODO: separate validation for server-to-server endpoints
const tokenlessUrls = ['login', 'updateGameParticipantCount', 'seedNBAPlayerData', 'health']

// THESE ARE ALL INTERNAL FUNCTIONS
exports.tokenAuth = async (token, url, method) => {
    const isPassUrl = tokenlessUrls.some((tlUrl) => url.includes(tlUrl))
    let userId = 0
    let userUid = ''

    if (!isPassUrl) {
        try {
            if ((url === '/users' && method === 'POST') || url === '/code/registration') {
                if (token !== loginTokenKey) throw true
            } else {
                const { userUid: verifiedUserUid } = jwt.verify(token, tokenKey)
                userId = await usersDAL.getUserIdFromToken(verifiedUserUid)
                userUid = verifiedUserUid
                if (!userId) throw true
            }
        } catch (e) {
            throw authError
        }
    }

    return { userId, userUid }
}

exports.createToken = async (userUid) => jwt.sign({ userUid }, tokenKey)
