ALTER TABLE users ADD UNIQUE (username);
ALTER TABLE users ADD UNIQUE (email);

ALTER TABLE user_codes ADD UNIQUE (user_id, type);

CREATE TABLE IF NOT EXISTS `user_followers` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) UNSIGNED NOT NULL,
    `following_id` int(11) UNSIGNED NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`following_id`) REFERENCES `users`(`id`) ON DELETE CASCADE,
    UNIQUE KEY `user_following_id` (`user_id`, `following_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `nba_game_links` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `nba_game_id` int(11) UNSIGNED NOT NULL,
    `game_link` varchar(300),
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`nba_game_id`) REFERENCES `nba_game_data`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE posts ADD COLUMN `body` text;
ALTER TABLE users MODIFY COLUMN `avatar_url` VARCHAR(200);