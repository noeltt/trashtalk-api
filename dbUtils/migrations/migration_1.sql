CREATE TABLE IF NOT EXISTS `nba_teams` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `nba_api_team_id` int(11) NOT NULL DEFAULT 0,  
    `full_name` varchar(100),
    `short_name` varchar(20), 
    `nickname` varchar(50),
    `conference` varchar(10),
    `logo_url` varchar(200),
    `big_image_url` varchar(200),
    `follow_count` int(11) DEFAULT 0,
    `points` int(11) NOT NULL DEFAULT 0,
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`), 
    UNIQUE KEY (`nba_api_team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `nba_team_details` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `team_id` int(11) unsigned NOT NULL DEFAULT 0,
    `nba_api_team_id` int(11) NOT NULL DEFAULT 0,  
    `win` int(11) NOT NULL DEFAULT 0,  
    `loss` int(11) NOT NULL DEFAULT 0,  
    `win_percentage` varchar(10),
    `loss_percentage` varchar(10),
    `last_ten_win` int(11) NOT NULL DEFAULT 0, 
    `last_ten_loss` int(11) NOT NULL DEFAULT 0, 
    `conference_rank` int(11) NOT NULL DEFAULT 0, 
    `conference_win` int(11) NOT NULL DEFAULT 0, 
    `conference_loss` int(11) NOT NULL DEFAULT 0, 
    `home_win` int(11) NOT NULL DEFAULT 0, 
    `home_loss` int(11) NOT NULL DEFAULT 0, 
    `away_win` int(11) NOT NULL DEFAULT 0, 
    `away_loss` int(11) NOT NULL DEFAULT 0, 
    `win_streak` int(11) NOT NULL DEFAULT 0,
    `games_behind` varchar(10),
    `points` int(11) NOT NULL DEFAULT 0,
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`), 
    UNIQUE KEY (`nba_api_team_id`),
    FOREIGN KEY (`team_id`) REFERENCES `nba_teams`(`id`) ON DELETE RESTRICT   
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELIMITER $$

CREATE TRIGGER `nba_team_to_team_details` AFTER INSERT ON `nba_teams`
  FOR EACH ROW BEGIN
    INSERT INTO `nba_team_details` SET `team_id` = NEW.id, `nba_api_team_id` = NEW.nba_api_team_id;
  END$$

DELIMITER ;

CREATE TABLE IF NOT EXISTS `nba_game_data` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `nba_api_game_id` int(11) UNSIGNED NOT NULL,  
    `start_time_utc` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `home_team_nba_api_id` int(11) DEFAULT 0, 
    `away_team_nba_api_id` int(11) DEFAULT 0, 
    `status` varchar(20),
    `clock` varchar(20),
    `quarter` int(11),
    `home_score` int(11) DEFAULT 0,
    `away_score` int(11) DEFAULT 0,
    `chat_participant_count` int(11) DEFAULT 0,
    PRIMARY KEY (`id`), 
    UNIQUE KEY (`nba_api_game_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

##REMEMBER ABOUT TEAM FOLLOW follow_count

CREATE TABLE IF NOT EXISTS `nba_team_followers` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `team_id` int(11) UNSIGNED NOT NULL,  
    `user_id` int(11) UNSIGNED NOT NULL, 
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`team_id`) REFERENCES `nba_teams`(`id`) ON DELETE RESTRICT,
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `post_poll_options` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `post_id` int(11) UNSIGNED NOT NULL,
    `option_id` int(11) DEFAULT 0, #0 - trash, 1 - like
    `option_name` varchar(100), 
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`post_id`) REFERENCES `posts`(`id`) ON DELETE RESTRICT    
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `post_poll_votes` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `post_id` int(11) UNSIGNED NOT NULL,  
    `option_id` int(11) UNSIGNED NOT NULL,
    `user_id` int(11) unsigned NOT NULL,
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`post_id`) REFERENCES `posts`(`id`) ON DELETE RESTRICT,
    UNIQUE KEY `poll_vote` (`post_id`, `user_id`)    
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE users ADD COLUMN bio varchar(400);

#---------------------------------------------- HERE NOW ON DEV

UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/AtlantaHawks.png' WHERE id = 1;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/CharlotteHornets.png' WHERE id = 4;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/DallasMavericks.png' WHERE id = 16;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/GoldenStateWarriors.png' WHERE id = 18;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/HoustonRockets.png' WHERE id = 19;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/LAClippers.png' WHERE id = 20;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/LALakers.png' WHERE id = 21;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/MemphisGrizzlies.png' WHERE id = 22;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/NOPelicans.png' WHERE id = 24;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/PhoenixSuns.png' WHERE id = 26;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/SacramentoKings.png' WHERE id = 28;
UPDATE nba_teams SET big_image_url='https://trashtalk-assets.s3.us-west-1.amazonaws.com/TeamRectanges/SASpurs.png' WHERE id = 29;

#---------------------------------------------- HERE NOW ON LOCAL

CREATE TABLE IF NOT EXISTS `nba_player_data` (   
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `first_name` varchar(100),  
    `last_name` varchar(100),  
    `full_name` varchar(150), 
    `team_id` int(11) UNSIGNED DEFAULT 0,
    `team_name` varchar(100),   
    `height` varchar(20),
    `position` varchar(100), 
    `position_short` varchar(10),
    `weight` varchar(20),
    `age` int(11) DEFAULT 0, 
    `jersey_number` varchar(20),
    `games_played` float DEFAULT 0,
    `minutes_per_game` float DEFAULT 0,
    `usage_percent` float DEFAULT 0,
    `turnover_percent` float DEFAULT 0,
    `freethrow_attempts` float DEFAULT 0,
    `freethrow_percent` float DEFAULT 0,
    `twopoint_attempts` float DEFAULT 0,
    `twopoint_percent` float DEFAULT 0,
    `threepoint_attempts` float DEFAULT 0,
    `threepoint_percent` float DEFAULT 0,
    `effective_field_goal_percent` float DEFAULT 0,
    `true_shooting_percent` float DEFAULT 0,
    `points_per_game` float DEFAULT 0,
    `rebounds_per_game` float DEFAULT 0,
    `total_rebound_percent` float DEFAULT 0,
    `assists_per_game` float DEFAULT 0,
    `assists_percent` float DEFAULT 0,
    `steals_per_game` float DEFAULT 0,
    `blocks_per_game` float DEFAULT 0,
    `turnover_per_game` float DEFAULT 0,
    `versatility_index` float DEFAULT 0,
    `offensive_rating` float DEFAULT 0,
    `defensive_rating` float DEFAULT 0,
    `headshot_url` varchar(300),
    `last_updated`TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE posts ADD COLUMN game_id int(11) DEFAULT 0;
ALTER TABLE posts ADD COLUMN team_id int(11) DEFAULT 0;
ALTER TABLE posts ADD COLUMN player_id int(11) DEFAULT 0;

#----------------------------------------------- HERE ON EVERYTHING