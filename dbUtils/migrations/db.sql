CREATE TABLE IF NOT EXISTS `users` (   
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,   
    `uid` varchar(100),
    `username` varchar(100),
    `password` varchar(512),
    `email` varchar(100),
    `avatar_url` varchar(100),
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, #MAKE SURE UTC 
    `verified` tinyint DEFAULT 0,   
    `admin` tinyint DEFAULT 0,   
    PRIMARY KEY (`id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `users_game_data` (   
    `user_id` int(11) UNSIGNED NOT NULL,
    `points` int(11) NOT NULL DEFAULT 0,
    #UNIQUE KEY (`order_id`, `package_name`, `product_id`, `purchase_token`),
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `user_codes` (   
    `user_id` int(11) UNSIGNED NOT NULL,
    `code` varchar(30),
    `type` int(11) DEFAULT 0,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, #MAKE SURE UTC 
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `posts` (   
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,  
    `uid` varchar(100), 
    `user_id` int(11) UNSIGNED NOT NULL,
    `title` varchar(100),
    `image_url` varchar(100),
    `post_type` int(11) DEFAULT 0,
    `comment_count` int(11) DEFAULT 0,
    `view_count` int(11) DEFAULT 0,
    `like_count` int(11) DEFAULT 0,
    `trash_count` int(11) DEFAULT 0,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, #MAKE SURE UTC 
    `active` tinyint DEFAULT 1,
    `points` int(11) DEFAULT 0,
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `post_comments` (  
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `uid` varchar(100),  
    `post_id` int(11) UNSIGNED NOT NULL,
    `user_id` int(11) UNSIGNED NOT NULL,
    `message` varchar(500),
    `reply_count` int(11) DEFAULT 0,
    `like_count` int(11) DEFAULT 0,
    `trash_count` int(11) DEFAULT 0,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, #MAKE SURE UTC 
    `points` int(11) DEFAULT 0,
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE RESTRICT,
    FOREIGN KEY (`post_id`) REFERENCES `posts`(`id`) ON DELETE RESTRICT 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `users_auth` (   
    `user_id` int(11) UNSIGNED NOT NULL,
    `token` varchar(500),
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, #MAKE SURE UTC 
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELIMITER $$

CREATE TRIGGER `users_game_data` AFTER INSERT ON `users`
  FOR EACH ROW BEGIN
    INSERT INTO `users_game_data` SET `user_id` = NEW.id;
  END$$

CREATE TRIGGER `users_auth_data` AFTER INSERT ON `users`
  FOR EACH ROW BEGIN
    INSERT INTO `users_auth` SET `user_id` = NEW.id;
  END$$

DELIMITER ;

CREATE TABLE IF NOT EXISTS `post_likes` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) UNSIGNED NOT NULL,  
    `post_id` int(11) UNSIGNED NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, #MAKE SURE UTC 
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`post_id`) REFERENCES `posts`(`id`) ON DELETE RESTRICT    
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `post_trash` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) UNSIGNED NOT NULL,  
    `post_id` int(11) UNSIGNED NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, #MAKE SURE UTC 
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`post_id`) REFERENCES `posts`(`id`) ON DELETE RESTRICT    
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `post_comment_likes` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) UNSIGNED NOT NULL,  
    `post_comment_id` int(11) UNSIGNED NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, #MAKE SURE UTC 
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`post_comment_id`) REFERENCES `post_comments`(`id`) ON DELETE RESTRICT   
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `post_comment_trash` ( 
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) UNSIGNED NOT NULL,  
    `post_comment_id` int(11) UNSIGNED NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, #MAKE SURE UTC 
    PRIMARY KEY (`id`), 
    FOREIGN KEY (`post_comment_id`) REFERENCES `post_comments`(`id`) ON DELETE RESTRICT   
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

###------------------------------ HERE NOW
