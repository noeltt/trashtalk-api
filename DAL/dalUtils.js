const mysql = require('mysql2/promise')

const config = require('../config.json')[global.env].dbConfig

const pool = mysql.createPool(config)

/* ---------------------------- Query shortcuts! ---------------------------- */

/**
 * Returns true if query results in one or more rows.
 */
exports.queryHasRows = async (query, values = []) => (await pool.query(query, values))[0].length > 0

/**
 * Returns the first row of the query result or `undefined` if there are no rows.
 */
exports.queryFirstRow = async (query, values = []) => (await pool.query(query, values))[0][0]
