const mysql = require('mysql2/promise')
const config = require('../config.json')[global.env].dbConfig
const pool = mysql.createPool(config)

const { queryHasRows, queryFirstRow } = require('./dalUtils')

// code type 0 - login
const codeTimeCheck = 'created_at > TIMESTAMPADD(HOUR, -1, CURRENT_TIMESTAMP)'

exports.verifyRegistrationCode = (code) =>
    queryFirstRow(`SELECT user_id FROM user_codes WHERE code = ? AND type=0 AND ${codeTimeCheck}`, [
        code,
    ])

exports.checkRegistrationCode = (code) =>
    queryHasRows(`SELECT 1 FROM user_codes WHERE code = ? AND type=0 AND ${codeTimeCheck}`, [code])

exports.addRegistrationCode = (userId, code) => {
    const query =
        'INSERT INTO user_codes (user_id, code, type) VALUES (?, ?, 0) ON DUPLICATE KEY UPDATE code = VALUES(code), created_at=CURRENT_TIMESTAMP;'
    return pool.query(query, [userId, code])
}

exports.deleteRegistrationCode = (userId) => {
    const query = 'DELETE FROM user_codes WHERE user_id = ? AND type = 0'
    pool.query(query, [userId])
}
