/*eslint-disable*/
const mysql = require('mysql2/promise')
const { queryHasRows, queryFirstRow } = require('./dalUtils')
const config = require('../config.json')[global.env].dbConfig

const pool = mysql.createPool(config)

//user tickets - all existing tickets - both submitted and not yet submitted
//raffles - all existing raffles, but closed and open
//raffle tickets - meeting of raffles and user tickets

// in the future, could be SELECT COUNT instead of EXISTS
exports.getCurrentRaffles = async (userId) =>
    pool.query(
        `SELECT r.*, (SELECT EXISTS (SELECT id FROM raffle_tickets WHERE user_id = ? and raffle_id = r.id)) as submitted FROM raffles r WHERE closes_at > TIMESTAMPADD(HOUR, -1, CURRENT_TIMESTAMP)`,
        [userId]
    )

// currently only checks for any
exports.getUserInRaffle = async (userId, raffleId) =>
    queryHasRows(`SELECT id FROM raffle_tickets WHERE user_id = ? AND raffle_id = ?`, [
        userId,
        raffleId,
    ])

exports.getUserAvailableRaffleTicket = async (userId) =>
    queryFirstRow(
        `SELECT id FROM user_raffle_tickets WHERE user_id = ? AND id NOT IN (SELECT ticket_id FROM raffle_tickets) LIMIT 1`,
        [userId]
    )

exports.getUserAvailableTicketCount = async (userId) =>
    queryFirstRow(
        `SELECT COUNT(*) as ticketCount FROM user_raffle_tickets WHERE user_id = ? AND id NOT IN (SELECT ticket_id FROM raffle_tickets)`,
        [userId]
    )

exports.addUserRaffleTicket = async (userId, raffleId, ticketId) => {
    await pool.query(
        `INSERT INTO raffle_tickets (user_id, raffle_id, ticket_id) VALUES (?, ?, ?)`,
        [userId, raffleId, ticketId]
    )
}
