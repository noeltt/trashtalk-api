/* eslint-disable */
// STATS relevant
// teams, players, games
const mysql = require('mysql2/promise')
const config = require('../config.json')[global.env].dbConfig

const { queryFirstRow, queryHasRows } = require('./dalUtils')

const pool = mysql.createPool(config)

// /////////////////////////////////
//       INTERNAL FUNCTIONS      //
// /////////////////////////////////
const checkIfUserFollowingTeam = async (userId, teamId) =>
    queryHasRows('SELECT id FROM nba_team_followers WHERE user_id = ? and team_id = ?', [
        userId,
        teamId,
    ])

// /////////////////////
//      TEAM CALLS    //
// /////////////////////
// should never have to be updated after seeding
exports.seedTeamBasics = async (seedData) =>
    pool.query(
        'INSERT INTO nba_teams (nba_api_team_id, logo_url, full_name, short_name, nickname, conference) VALUES ? ',
        [seedData]
    )

exports.updateTeamDetails = async (teamDetails) => {
    let query = `INSERT INTO nba_team_details 
        (nba_api_team_id, win, loss, win_percentage, loss_percentage, last_ten_win, last_ten_loss, 
            conference_rank, conference_win, conference_loss, home_win, home_loss, 
            away_win, away_loss, win_streak, games_behind) VALUES ? `

    query += `ON DUPLICATE KEY UPDATE 
        win = VALUES(win),
        loss = VALUES(loss),
        win_percentage = VALUES(win_percentage),
        loss_percentage = VALUES(loss_percentage),
        last_ten_win = VALUES(last_ten_win),
        last_ten_loss = VALUES(last_ten_loss),
        conference_rank = VALUES(conference_rank),
        conference_win = VALUES(conference_win),
        conference_loss = VALUES(conference_loss),
        home_win = VALUES(home_win),
        home_loss = VALUES(home_loss),
        away_win = VALUES(away_win),
        away_loss = VALUES(away_loss),
        win_streak = VALUES(win_streak),
        games_behind = VALUES(games_behind);`

    await pool.query(query, [teamDetails])
}

exports.getTeamList = async (userId) => {
    const query = `SELECT nt.*, 
        (SELECT 1 as this_exists FROM nba_team_followers WHERE user_id = ? and team_id = nt.id) as following 
        FROM nba_teams nt;`
    return pool.query(query, [userId])
}

exports.getTeamData = async (userId, teamId) => {
    const query = `SELECT *,
        (SELECT 1 as this_exists FROM nba_team_followers WHERE user_id = ? and team_id = nt.id) as following 
        FROM nba_teams nt 
        LEFT JOIN nba_team_details ntd ON nt.id = ntd.team_id 
        WHERE nt.id = ?`
    return queryFirstRow(query, [userId, teamId])
}

exports.getFollowingTeams = async (userId) =>
    pool.query('SELECT DISTINCT team_id FROM nba_team_followers WHERE user_id = ?', [userId])

exports.getFollowingTeamData = async (userId) => {
    const query = `SELECT nt.logo_url as logoUrl, nt.id as teamId
        FROM nba_team_followers ntf 
        LEFT JOIN nba_teams nt ON ntf.team_id = nt.id
        WHERE ntf.user_id = ?`

    return pool.query(query, [userId])
}

exports.followTeam = async (userId, teamId) => {
    if (!(await checkIfUserFollowingTeam(userId, teamId))) {
        await pool.query('INSERT INTO nba_team_followers (user_id, team_id) VALUE (?, ?)', [
            userId,
            teamId,
        ])
        await pool.query('UPDATE nba_teams SET follow_count=follow_count+1 WHERE id = ?', [teamId])
    }
}

exports.unfollowTeam = async (userId, teamId) => {
    if (await checkIfUserFollowingTeam(userId, teamId)) {
        await pool.query('DELETE FROM nba_team_followers WHERE user_id = ? AND team_id = ?', [
            userId,
            teamId,
        ])
        await pool.query(
            'UPDATE nba_teams SET follow_count=GREATEST(follow_count-1, 0) WHERE id = ?',
            [teamId]
        )
    }
}

// /////////////////////
//      GAME CALLS    //
// /////////////////////
exports.upsertNbaGames = async (gameDetails) => {
    // await pool.query(`UPDATE nba_game_data SET status='finished' WHERE status != 'scheduled'`)

    // some sort of strange error going on here
    const query = `INSERT INTO nba_game_data 
        (nba_api_game_id, start_time_utc, home_team_nba_api_id, away_team_nba_api_id, status, clock, quarter, home_score, away_score) 
        VALUES ? ON DUPLICATE KEY UPDATE
            status = VALUES(status),
            quarter = VALUES(quarter),
            clock = VALUES(clock),
            home_score = VALUES(home_score),
            away_score = VALUES(away_score)`

    await pool.query(query, [gameDetails])
}

exports.getUpcomingGames = async () =>
    pool.query(
        `SELECT ngd.id, ngd.start_time_utc, ngd.status, ngd.quarter, ngd.clock, ngd.home_score, ngd.away_score, ngd.chat_participant_count,
        hnt.nickname as home_team_name, hnt.short_name as home_short_name, hnt.logo_url as home_team_logo, 
        ant.nickname as away_team_name, ant.short_name as away_short_name, ant.logo_url as away_team_logo,
        htd.win as home_win, htd.loss as home_loss,
        atd.win as away_win, atd.loss as away_loss
        FROM nba_game_data ngd 
        LEFT JOIN nba_teams hnt ON ngd.home_team_nba_api_id = hnt.nba_api_team_id 
            LEFT JOIN nba_team_details htd ON hnt.id = htd.team_id
        LEFT JOIN nba_teams ant ON ngd.away_team_nba_api_id = ant.nba_api_team_id 
            LEFT JOIN nba_team_details atd ON ant.id = atd.team_id
        WHERE ngd.status != 'finished' AND ngd.start_time_utc > TIMESTAMPADD(HOUR, -10, UTC_TIMESTAMP());`
    )

exports.getPreviousGames = async () =>
    pool.query(
        `SELECT ngd.id, ngd.start_time_utc, ngd.status, ngd.quarter, ngd.clock, ngd.home_score, ngd.away_score, ngd.chat_participant_count,
    hnt.nickname as home_team_name, hnt.short_name as home_short_name, hnt.logo_url as home_team_logo, 
    ant.nickname as away_team_name, ant.short_name as away_short_name, ant.logo_url as away_team_logo,
    htd.win as home_win, htd.loss as home_loss,
    atd.win as away_win, atd.loss as away_loss
    FROM nba_game_data ngd 
    LEFT JOIN nba_teams hnt ON ngd.home_team_nba_api_id = hnt.nba_api_team_id 
        LEFT JOIN nba_team_details htd ON hnt.id = htd.team_id
    LEFT JOIN nba_teams ant ON ngd.away_team_nba_api_id = ant.nba_api_team_id 
        LEFT JOIN nba_team_details atd ON ant.id = atd.team_id
    WHERE ngd.status = 'finished' OR ngd.status = 'after over time' ORDER BY ngd.id DESC LIMIT 20;`
    )

exports.getGameData = async (gameId) =>
    queryFirstRow(
        `SELECT ngd.id, ngd.start_time_utc, ngd.quarter, ngd.clock, ngd.home_score, ngd.away_score, ngd.chat_participant_count,
        hnt.id as home_team_id, hnt.nickname as home_team_name, hnt.short_name as home_short_name, hnt.logo_url as home_team_logo, 
        ant.id as away_team_id, ant.nickname as away_team_name, ant.short_name as away_short_name, ant.logo_url as away_team_logo,
        htd.win as home_win, htd.loss as home_loss,
        atd.win as away_win, atd.loss as away_loss
        FROM nba_game_data ngd 
        LEFT JOIN nba_teams hnt ON ngd.home_team_nba_api_id = hnt.nba_api_team_id 
            LEFT JOIN nba_team_details htd ON hnt.id = htd.team_id
        LEFT JOIN nba_teams ant ON ngd.away_team_nba_api_id = ant.nba_api_team_id 
            LEFT JOIN nba_team_details atd ON ant.id = atd.team_id
        WHERE ngd.id = ?;`,
        [gameId]
    )

exports.getGameLinks = async (gameId) =>
    pool.query(
        `SELECT game_link FROM nba_game_links WHERE nba_game_id = ? ORDER BY id DESC LIMIT 3`,
        [gameId]
    )

exports.getLiveGameStats = async () => {
    // lint
    return pool.query(
        `SELECT id, home_score, away_score, quarter, clock, chat_participant_count 
        FROM nba_game_data 
        WHERE status='q1' OR
        status='q2' OR
        status='q3' OR
        status='q4' OR
        status='ot' OR
        status='bt' OR
        status='ht'`
    )
}

exports.updateGameParticipantCount = async (gameId, participantCount) => {
    const query = `UPDATE nba_game_data SET chat_participant_count = ? WHERE id = ?`
    await pool.query(query, [participantCount, gameId])
}

/////////////////////////////////
//       PLAYER CALLS          //
////////////////////////////////

exports.seedNBAPlayers = async (players) => {
    const query = `INSERT INTO nba_player_data 
        (first_name, last_name, full_name, team_name, position, height, weight, jersey_number, age, headshot_url ) VALUES ?`

    await pool.query(query, [players])
}

exports.nbaTeamPlayerAllocate = async () => {
    const query = `UPDATE nba_player_data npd 
        SET team_id = (
                SELECT id 
                FROM nba_teams 
                WHERE full_name=npd.team_name LIMIT 1
            );`

    await pool.query(query)
}

exports.updatePlayerData = async (player) => {
    const query = `UPDATE nba_player_data
        SET position_short = ?, games_played = ?, minutes_per_game = ?, usage_percent = ?,
            turnover_percent = ?, freethrow_attempts = ?, freethrow_percent = ?, twopoint_attempts = ?,
            twopoint_percent = ?, threepoint_attempts = ?, threepoint_percent = ?, effective_field_goal_percent = ?,
            true_shooting_percent = ?, points_per_game = ?, rebounds_per_game = ?, total_rebound_percent = ?,
            assists_per_game = ?, assists_percent = ?, steals_per_game = ?, blocks_per_game = ?, turnover_per_game = ?,
            versatility_index = ?, offensive_rating = ?, defensive_rating = ?
        WHERE full_name = ?`

    await pool.query(query, player)
}

exports.getTeamPlayers = async (teamId) => {
    const query = `SELECT *
        FROM nba_player_data
        WHERE team_id = ?
        ORDER BY games_played DESC`

    return pool.query(query, teamId)
}

exports.getPlayerData = async (playerId) => {
    const query = `SELECT npd.*, nt.big_image_url 
        FROM nba_player_data npd
        LEFT JOIN nba_teams nt ON npd.team_id = nt.id
        WHERE npd.id = ?`

    return pool.query(query, playerId)
}

exports.findPlayerData = async (playerName) => {
    const query = `SELECT npd.*, nt.big_image_url, nt.full_name as team_full_name
        FROM nba_player_data npd
        LEFT JOIN nba_teams nt ON npd.team_id = nt.id
        WHERE nt.full_name IS NOT NULL AND (npd.first_name LIKE CONCAT(? , '%') OR npd.last_name LIKE CONCAT(? , '%'))
        LIMIT 20`

    return pool.query(query, [playerName, playerName])
}

exports.findDefaultPlayerData = async () => {
    const query = `SELECT npd.*, nt.big_image_url, nt.full_name as team_full_name
    FROM nba_player_data npd
    LEFT JOIN nba_teams nt ON npd.team_id = nt.id
    WHERE npd.id IN (1320, 1240, 1477, 1445, 1610, 1549, 1598, 1210, 1206, 928, 1433, 1382, 1057, 1613, 1340)`

    return pool.query(query)
}
