/* eslint-disable */
const mysql = require('mysql2/promise')
const config = require('../config.json')[global.env].dbConfig
const pool = mysql.createPool(config)

const { queryHasRows, queryFirstRow } = require('./dalUtils')

// password must be hashed
exports.checkLogin = async (email) => {
    const [data] = await pool.query(
        'SELECT uid, username, password, avatar_url FROM users WHERE email = ? AND verified=1;',
        [email]
    )
    if (data.length > 0)
        return {
            userUid: data[0].uid,
            username: data[0].username,
            hashPassword: data[0].password,
            avatarUrl: data[0].avatar_url,
        }
    return {
        userUid: false,
    }
}

exports.getUserIdFromUid = async (uid) => {
    const query = `SELECT id FROM users WHERE uid = ?`
    const ret = await queryFirstRow(query, [uid])
    return ret !== undefined ? ret.id : 0
}

exports.getUserDetails = async (userId) => {
    const query = `SELECT username, bio, avatar_url as avatarUrl 
        FROM users 
        WHERE id=?`

    return queryFirstRow(query, [userId])
}

exports.getUserProfileCounts = async (userId) => {
    const { postCount = 0 } = await queryFirstRow(
        `SELECT COUNT(*) as postCount FROM posts WHERE user_id = ?`,
        [userId]
    )

    const subPostCounts = await queryFirstRow(
        `SELECT user_id, SUM(comment_count) as commentCount, SUM(like_count) as likeCount, SUM(trash_count) as trashCount FROM posts WHERE user_id=? GROUP BY user_id`,
        [userId]
    )

    const totalCommentCount = parseInt(subPostCounts?.commentCount ?? 0)
    const totalLikeCount = parseInt(subPostCounts?.likeCount ?? 0)
    const totalTrashCount = parseInt(subPostCounts?.trashCount ?? 0)

    return { postCount, totalCommentCount, totalLikeCount, totalTrashCount }
}

exports.getUserTopPosts = async (userId, profileUserId) => {
    const query = `SELECT p.*, 
    u.username, u.uid, u.avatar_url as avatarUrl, u.admin as isAdmin,
    (SELECT EXISTS (SELECT id FROM post_likes pl WHERE pl.post_id = p.id AND pl.user_id = ${userId})) as userLike,
    (SELECT EXISTS (SELECT id FROM post_trash pt WHERE pt.post_id = p.id AND pt.user_id = ${userId})) as userTrash
    FROM posts p 
    LEFT JOIN users u ON p.user_id = u.id 
    WHERE p.user_id = ?
    ORDER BY p.points DESC, p.id DESC
    LIMIT 3`

    return pool.query(query, [profileUserId])
}

exports.getUserIdFromToken = async (userUid) => {
    const query = `SELECT id 
        FROM users 
        WHERE uid = ?`

    let { id } = await queryFirstRow(query, [userUid])
    return id
}

exports.createUser = async (email, username, hashPassword, uid) => {
    const query = `INSERT INTO users (email, username, password, uid)
        VALUES (?, ?, ?, ?)`
    return pool.query(query, [email, username, hashPassword, uid])
}

exports.giveBaseRaffleTickets = async (userId) => {
    const query = `INSERT INTO user_raffle_tickets (user_id) VALUES (${userId}), (${userId}), (${userId})`
    await pool.query(query)
}

exports.verifyUser = async (userId) => {
    const query = 'UPDATE users SET verified = 1 WHERE id = ?'
    await pool.query(query, [userId])
}

exports.usernameExists = async (username, userId) =>
    queryHasRows('SELECT 1 as this_exists FROM users WHERE username = ? AND id != ?', [
        username,
        userId,
    ])

exports.updateUsername = async (username, userId) =>
    pool.query('UPDATE users SET username = ? WHERE id = ?', [username, userId])

exports.updateUserBio = async (bio, userId) =>
    pool.query('UPDATE users SET bio = ? WHERE id = ?', [bio, userId])

exports.followUser = async (userId, followingUserId) =>
    pool.query(`INSERT IGNORE INTO user_followers (user_id, following_id) VALUES (?, ?)`, [
        userId,
        followingUserId,
    ])

exports.unfollowUser = async (userId, followingUserId) => {
    const query = `DELETE FROM user_followers WHERE user_id = ? AND following_id = ?`
    await pool.query(query, [userId, followingUserId])
}

exports.getUserFollowing = async (userId) => {
    const query = `SELECT u.uid, u.username, u.avatar_url FROM user_followers uf 
    LEFT JOIN users u ON uf.following_id = u.id 
    WHERE uf.user_id = ?`

    return pool.query(query, [userId])
}

exports.findUserByUsername = async (username, userId) => {
    const query = `SELECT u.uid, u.username, u.avatar_url, (uf.id > 0) as followingRaw
    FROM users u 
    LEFT JOIN user_followers uf ON u.id = uf.following_id
    WHERE u.id != ?
    AND u.username LIKE CONCAT(? , '%') AND u.verified = 1 
    LIMIT 30`
    return pool.query(query, [userId, username])
}

exports.updateUserProfileURL = async (userId, url) => {
    const query = `UPDATE users SET avatar_url = ? WHERE id = ?`
    await pool.query(query, [url, userId])
}

exports.anonymizeUser = async (userId) => {
    const temp = Math.random().toString(20).toLowerCase()
    await pool.query(`UPDATE users SET username='${temp}', email='${temp}' WHERE id=?`, [userId])
}
