/* eslint-disable */
const mysql = require('mysql2/promise')
const config = require('../config.json')[global.env].dbConfig
const pool = mysql.createPool(config)

const { queryHasRows, queryFirstRow } = require('./dalUtils')

// /////////////////////////////////
//       INTERNAL FUNCTIONS      //
// /////////////////////////////////

const checkUserPostReacts = async (userId, postId) => ({
    userTrashPost: await queryHasRows(
        'SELECT id FROM post_trash WHERE user_id = ? and post_id = ?',
        [userId, postId]
    ),
    userLikePost: await queryHasRows(
        'SELECT id FROM post_likes WHERE user_id = ? and post_id = ?',
        [userId, postId]
    ),
})

const checkUserCommentReacts = async (userId, commentId) => ({
    userTrashComment: await queryHasRows(
        'SELECT id FROM post_comment_trash WHERE user_id = ? and post_comment_id = ?',
        [userId, commentId]
    ),
    userLikeComment: await queryHasRows(
        'SELECT id FROM post_comment_likes WHERE user_id = ? and post_comment_id = ?',
        [userId, commentId]
    ),
})
// //////////////////////////////
//        REPORTING LEVEL      //
// //////////////////////////////

exports.reportPost = async (userId, postId) => {
    await pool.query(`INSERT INTO user_post_reports (user_id, post_id) VALUES (?, ?)`, [
        userId,
        postId,
    ])
}

exports.hidePost = async (userId, postId) => {
    await pool.query(`INSERT INTO user_post_hides (user_id, post_id) VALUES (?, ?)`, [
        userId,
        postId,
    ])
}

exports.hideUser = async (userId, hideUserId) => {
    await pool.query(`INSERT INTO user_user_hides (user_id, hidden_user_id) VALUES (?, ?)`, [
        userId,
        hideUserId,
    ])
}

// //////////////////////////////
//        POSTS LEVEL          //
// //////////////////////////////

exports.doesPostExist = async (postId) =>
    queryHasRows('SELECT 1 as thisExists FROM posts WHERE id = ?', [postId])

exports.createPost = async ({
    postType,
    pollOptions,
    postTitle,
    postBody,
    userId,
    postImageUrl,
    gameId,
    teamId,
    playerId,
}) => {
    const query = `INSERT INTO posts (post_type, title, body, user_id, image_url, game_id, team_id, player_id, created_at)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP())`

    const postInsert = await pool.query(query, [
        postType,
        postTitle,
        postBody,
        userId,
        postImageUrl,
        gameId,
        teamId,
        playerId,
    ])

    // console.log({ postType, pollOptions })
    //1 - poll post
    if (postType === 1) {
        const { insertId: postId } = postInsert[0]
        let optionsQuery = `INSERT INTO post_poll_options (post_id, option_id, option_name) VALUES `

        pollOptions.forEach((option, i) => {
            optionsQuery += `(${postId}, ${i + 1}, "${option}")`
            optionsQuery += i < pollOptions.length - 1 ? `, ` : `; `
        })

        await pool.query(optionsQuery)
    }
}

exports.voteForPoll = async (postId, optionId, userId) => {
    await pool.query('INSERT INTO post_poll_votes (post_id, option_id, user_id) VALUES (?, ?, ?)', [
        postId,
        optionId,
        userId,
    ])
}

// just feels cleaner to read
exports.getPostDetails = (postId, userId) =>
    queryFirstRow(
        `SELECT p.*, 
    u.username, u.avatar_url as avatarUrl, u.admin as isAdmin, u.uid as author_uid,
    pl.user_id as userLike,
    pt.user_id as userTrash
    FROM posts p 
    LEFT JOIN users u ON p.user_id = u.id 
    LEFT JOIN post_likes pl ON pl.post_id = p.id AND pl.user_id = ?
    LEFT JOIN post_trash pt ON pt.post_id = p.id AND pt.user_id = ?
    WHERE p.id = ?`,
        [userId, userId, postId]
    )

exports.getPostOptions = (postId) => {
    const query = `SELECT option_id as id, option_name as name 
        FROM post_poll_options
        WHERE post_id = ?
        ORDER BY option_id`

    return pool.query(query, [postId])
}

exports.getPostVotes = (postId) => {
    const query = `SELECT COUNT(id) as voteCount, option_id as optionId 
        FROM post_poll_votes 
        WHERE post_id = ? 
        GROUP BY option_id;`

    return pool.query(query, [postId])
}

exports.didUserVote = (postId, userId) => {
    const query = `SELECT option_id FROM post_poll_votes WHERE post_id = ? and user_id = ?`
    return pool.query(query, [postId, userId])
}

exports.likePost = async (userId, postId) => {
    const { userTrashPost, userLikePost } = await checkUserPostReacts(userId, postId)

    if (!userLikePost) {
        await pool.query('INSERT INTO post_likes (user_id, post_id) VALUES (?, ?)', [
            userId,
            postId,
        ])
    }

    if (userTrashPost) {
        await pool.query('DELETE FROM post_trash WHERE user_id = ? and post_id = ? ', [
            userId,
            postId,
        ])
    }

    // if either condition is true then count needs to be updated
    if (!userLikePost || userTrashPost) {
        let query = 'UPDATE posts SET '
        query += !userLikePost ? 'like_count=like_count+1' : ''
        query += !userLikePost && userTrashPost ? ', ' : ''
        query += userTrashPost ? 'trash_count=GREATEST(trash_count-1, 0)' : ''
        query += ' WHERE id = ?'

        await pool.query(query, [postId])
    }
}

exports.trashPost = async (userId, postId) => {
    // check if user reacts already exist
    const { userTrashPost, userLikePost } = await checkUserPostReacts(userId, postId)

    if (!userTrashPost) {
        await pool.query('INSERT INTO post_trash (user_id, post_id) VALUES (?, ?)', [
            userId,
            postId,
        ])
    }

    if (userLikePost) {
        await pool.query('DELETE FROM post_likes WHERE user_id = ? and post_id = ? ', [
            userId,
            postId,
        ])
    }

    // if either condition is true then count needs to be updated
    if (userLikePost || !userTrashPost) {
        let query = 'UPDATE posts SET '
        query += !userTrashPost ? 'trash_count=trash_count+1' : ''
        query += userLikePost && !userTrashPost ? ', ' : ''
        query += userLikePost ? 'like_count=GREATEST(like_count-1, 0)' : ''
        query += ' WHERE id = ?'

        await pool.query(query, [postId])
    }
}

exports.unlikePost = async (userId, postId) => {
    // check if user reacts already exist
    const { userLikePost } = await checkUserPostReacts(userId, postId)

    if (!userLikePost) return

    await pool.query('UPDATE posts SET like_count=GREATEST(like_count - 1, 0) WHERE id = ? ', [
        postId,
    ])
    await pool.query('DELETE FROM post_likes WHERE post_id = ? and user_id = ?', [postId, userId])
}

exports.untrashPost = async (userId, postId) => {
    // check if user reacts already exist
    const { userTrashPost } = await checkUserPostReacts(userId, postId)

    if (!userTrashPost) {
        return
    }

    await pool.query('UPDATE posts SET trash_count=GREATEST(trash_count - 1, 0) WHERE id = ?', [
        postId,
    ])
    await pool.query('DELETE FROM post_trash WHERE post_id = ? and user_id = ?', [postId, userId])
}

exports.getPosts = async (gameId = '', teamId = '', playerId = '', userId) => {
    const whereClause =
        gameId.length > 0
            ? 'WHERE p.game_id = ?'
            : teamId.length > 0
            ? 'WHERE p.team_id = ?'
            : playerId.length > 0
            ? 'WHERE p.player_id = ?'
            : ''

    const secondaryWhere = whereClause.length > 0 ? 'AND' : 'WHERE'

    const params =
        gameId.length > 0
            ? gameId
            : teamId.length > 0
            ? teamId
            : playerId.length > 0
            ? playerId
            : ''

    //test
    const query = `SELECT p.*, 
    u.username, u.uid, u.avatar_url as avatarUrl, u.admin as isAdmin,
    (SELECT EXISTS (SELECT id FROM post_likes pl WHERE pl.post_id = p.id AND pl.user_id = ${userId})) as userLike,
    (SELECT EXISTS (SELECT id FROM post_trash pt WHERE pt.post_id = p.id AND pt.user_id = ${userId})) as userTrash
    FROM posts p 
    LEFT JOIN users u ON p.user_id = u.id 
        ${whereClause} ${secondaryWhere} 
            p.id NOT IN (SELECT post_id FROM user_post_hides WHERE user_id=${userId})
            AND p.user_id NOT IN (SELECT hidden_user_id from user_user_hides WHERE user_id=${userId})
        ORDER BY p.id DESC
        LIMIT 20`

    return pool.query(query, [params])
}

exports.addPointsToPost = async (postId, points) => {
    const query = `UPDATE posts SET points = (points + ?) WHERE id = ?`
    await pool.query(query, [points, postId])
}

exports.subtractPointsFromPost = async (postId, points) => {
    const query = `UPDATE posts SET points = GREATEST(points - ?, 0) WHERE id = ?`
    await pool.query(query, [points, postId])
}

// HERE
exports.getUserPostReactions = async (userId, postIds) => {
    const joinedIds = `(${postIds.join(',')})`
}

exports.getPollPostsOptions = async (userId, postIds) => {
    const joinedIds = `(${postIds.join(',')})`
    const query = `SELECT ppo.post_id as postId, ppo.option_id as optionId, ppo.option_name as optionName,
    (SELECT COUNT(ppv.id) FROM post_poll_votes ppv WHERE ppo.post_id = ppv.post_id AND ppv.option_id = ppo.option_id) as voteCount,
    (SELECT EXISTS (SELECT id FROM post_poll_votes ppv WHERE ppo.post_id = ppv.post_id AND ppv.user_id = ${userId} AND ppv.option_id=ppo.option_id)) as didUserVote
    FROM post_poll_options ppo
    WHERE post_id IN ${joinedIds}
    ORDER BY option_id`

    return pool.query(query)
}

exports.getHotPosts = async (userId) => {
    const query = `SELECT p.*, 
        u.username, u.uid, u.avatar_url as avatarUrl, u.admin as isAdmin,
        (SELECT EXISTS (SELECT id FROM post_likes pl WHERE pl.post_id = p.id AND pl.user_id = ${userId})) as userLike,
        (SELECT EXISTS (SELECT id FROM post_trash pt WHERE pt.post_id = p.id AND pt.user_id = ${userId})) as userTrash
        FROM posts p 
        LEFT JOIN users u ON p.user_id = u.id 
        WHERE p.created_at > UTC_TIMESTAMP() - INTERVAL 2 WEEK
        ORDER BY p.points DESC, p.id DESC
        LIMIT 20`
    // limit in last week
    return pool.query(query)
}
// exports.getHottestPosts = async() =>

//
//
// //////////////////////////////
//        COMMENTS LEVEL       //
// //////////////////////////////

exports.doesCommentExist = async (commentId) =>
    queryHasRows('SELECT 1 as thisExists FROM post_comments WHERE id = ?', [commentId])
exports.createPostComment = async (postId, { userId, message }) => {
    await pool.query(
        'INSERT INTO post_comments (post_id, user_id, message, created_at) VALUES (?, ?, ?, UTC_TIMESTAMP())',
        [postId, userId, message]
    )
    await pool.query('UPDATE posts SET comment_count = comment_count+1 WHERE id = ?', [postId])
}

exports.likeComment = async (userId, commentId) => {
    // check if user reacts already exist
    const { userTrashComment, userLikeComment } = await checkUserCommentReacts(userId, commentId)

    if (!userLikeComment) {
        await pool.query(
            'INSERT INTO post_comment_likes (user_id, post_comment_id) VALUES (?, ?)',
            [userId, commentId]
        )
    }

    if (userTrashComment) {
        await pool.query(
            'DELETE FROM post_comment_trash WHERE user_id = ? and post_comment_id = ? ',
            [userId, commentId]
        )
    }

    // if either condition is true then count needs to be updated
    if (!userLikeComment || userTrashComment) {
        let query = 'UPDATE post_comments SET '
        query += !userLikeComment ? 'like_count=like_count+1' : ''
        query += !userLikeComment && userTrashComment ? ', ' : ''
        query += userTrashComment ? 'trash_count=GREATEST(trash_count-1, 0)' : ''
        query += ' WHERE id = ?'

        await pool.query(query, [commentId])
    }
}
exports.trashComment = async (userId, commentId) => {
    // check if user reacts already exist
    const { userTrashComment, userLikeComment } = await checkUserCommentReacts(userId, commentId)

    if (!userTrashComment) {
        await pool.query(
            'INSERT INTO post_comment_trash (user_id, post_comment_id) VALUES (?, ?)',
            [userId, commentId]
        )
    }

    if (userLikeComment) {
        await pool.query(
            'DELETE FROM post_comment_likes WHERE user_id = ? and post_comment_id = ? ',
            [userId, commentId]
        )
    }

    // if either condition is true then count needs to be updated
    if (userLikeComment || !userTrashComment) {
        let query = 'UPDATE post_comments SET '
        query += !userTrashComment ? 'trash_count=trash_count+1' : ''
        query += userLikeComment && !userTrashComment ? ', ' : ''
        query += userLikeComment ? 'like_count=GREATEST(like_count-1, 0)' : ''
        query += ' WHERE id = ?'

        await pool.query(query, [commentId])
    }
}

exports.unlikeComment = async (userId, commentId) => {
    const { userLikeComment } = await checkUserCommentReacts(userId, commentId)

    if (!userLikeComment) return

    await pool.query('DELETE FROM post_comment_likes WHERE user_id =? AND post_comment_id = ?', [
        userId,
        commentId,
    ])
    await pool.query(
        'UPDATE post_comments SET like_count=(GREATEST(like_count-1, 0)) WHERE id = ?',
        [commentId]
    )
}

exports.untrashComment = async (userId, commentId) => {
    const { userTrashComment } = await checkUserCommentReacts(userId, commentId)

    if (!userTrashComment) return

    await pool.query('DELETE FROM post_comment_trash WHERE user_id =? AND post_comment_id = ? ', [
        userId,
        commentId,
    ])
    await pool.query(
        'UPDATE post_comments SET trash_count=(GREATEST(trash_count-1, 0)) WHERE id = ?',
        commentId
    )
}

exports.getPostComments = async (userId, postId) => {
    const query = `SELECT pc.*, 
        u.username, u.avatar_url as avatarUrl, 
        pcl.user_id as userLike,
        pct.user_id as userTrash
        FROM post_comments pc 
        LEFT JOIN users u ON pc.user_id = u.id 
        LEFT JOIN post_comment_likes pcl ON pcl.post_comment_id = pc.id AND pcl.user_id = ?
        LEFT JOIN post_comment_trash pct ON pct.post_comment_id = pc.id AND pct.user_id = ?
        WHERE pc.post_id = ?
        ORDER BY pc.id DESC
        LIMIT 30`

    return pool.query(query, [userId, userId, postId])
}

//
//
// //////////////////////////////
//   POST REPLIES LEVEL       //
// //////////////////////////////
