/*eslint-disable*/
const router = require('koa-router')()
const posts = require('../services/posts')
const nbaStats = require('../services/nbaStats')
const users = require('../services/users')
const raffles = require('../services/raffles')
const serverFunc = require('../services/serverFunctions')
const seeder = require('../statsUtil/nba')
const playerSeeder = require('../statsUtil/nbaPlayers')

exports.routes = router
    .get('/health', serverFunc.health)

    // /////////////////////////
    //       USER ENDPOINTS   //
    // /////////////////////////
    .post('/login', users.login)

    .get('/users', users.findUser)
    .post('/users', users.createUser)
    .post('/users/deleteme', users.deleteMe)

    .post('/users/following', users.followUser)
    .del('/users/following', users.unfollowUser)
    .get('/users/following', users.getFollowing)

    // .get('/users/details', users.getUserDetails)
    .put('/users/details', users.updateUserDetails)
    .put('/users/profileImage', users.updateUserProfilePicture)

    // TODO: rename all these endpoints
    .get('/users/profile', users.getUserProfile)
    .get('/users/posts', users.getUserTopPosts)
    .get('/teams/profile', nbaStats.getProfileFollowingTeams)

    // user profile - not until later

    // /////////////////////////
    //       CODE ENDPOINTS   //
    // /////////////////////////

    .put('/code/registration', users.verifyRegistrationCode)

    // //////////////////
    // POST ENDPOINTS  //
    // //////////////////

    // POST
    .post('/posts', posts.createPost)
    .post('/posts/:postId/like', posts.likePost)
    .post('/posts/:postId/trash', posts.trashPost)
    .post('/posts/:postId/votes', posts.voteForPoll)
    .post('/posts/image', users.addImage)

    .put('/posts/:postId/unlike', posts.unlikePost)
    .put('/posts/:postId/untrash', posts.untrashPost)

    // PUT - need view call
    // .put('')

    // GET
    .get('/posts/latest', posts.getPosts)
    .get('/posts/hot', posts.getHotPosts)
    .get('/posts', posts.getPosts)
    .get('/posts/:postId', posts.getPostDetails)
    .get('/posts/:postId/votes', posts.getPostPollResults)

    // /////////////////////
    //  REPORT ENDPOINTS  //
    ////////////////////////
    .post('/report/post', posts.reportPost)
    .post('/block/post', posts.hidePost)
    .post('/block/user', posts.blockUser)

    // /////////////////////
    // COMMENT ENDPOINTS  //
    // /////////////////////
    .post('/posts/:postId/comments', posts.createComment)
    .post('/comments/:commentId/like', posts.likePostComment)
    .post('/comments/:commentId/trash', posts.trashPostComment)

    .put('/comments/:commentId/unlike', posts.unlikePostComment)
    .put('/comments/:commentId/untrash', posts.untrashPostComment)

    .get('/posts/:postId/comments', posts.getPostComments)

    // /////////////////////
    //   GAME ENDPOINTS   //
    // /////////////////////
    .get('/games/upcoming', nbaStats.getUpcomingGames)
    .get('/games/previous', nbaStats.getPreviousGames)
    .get('/games/:gameId', nbaStats.getGameData)
    .get('/games/:gameId/links', nbaStats.getGameLinks)

    // /////////////////////
    //   TEAM ENDPOINTS   //
    // /////////////////////
    .post('/teams/:teamId/follow', nbaStats.followTeam)

    .put('/teams/:teamId/unfollow', nbaStats.unfollowTeam)

    .get('/teams', nbaStats.getTeams)
    .get('/teams/follow', nbaStats.getFollowingTeams)
    .get('/teams/:teamId', nbaStats.getTeamData)

    // /////////////////////
    //  PLAYER ENDPOINTS   //
    // /////////////////////

    .get('/teams/:teamId/players', nbaStats.getTeamPlayers)
    .get('/players/:playerId', nbaStats.getPlayerData)
    .get('/nba/players', nbaStats.findPlayer)
    .get('/test', nbaStats.test)

    // ////////////////////////////
    //       RAFFLES ENDPOINTS   //
    // ////////////////////////////

    .get('/raffles', raffles.getCurrentRaffles)
    .post('/raffles', raffles.submitRaffleTicket)

    // INTERNAL USE ONLY //
    .get('/seedTeams', seeder.seedJobNBA)
    // TEMPORARY //
    .get('/seedGameData', seeder.getUpcomingGameDetails)
    .get('/seedNBAPlayerData', playerSeeder.loadPlayersFromAPI)

    .post('/testGameDataEmit', seeder.testEmitLiveGameData)

    // ///////////////////////
    //  INTERNAL ENDPOINTS  //
    // ///////////////////////

    .put('/updateGameParticipantCount', nbaStats.updateGameParticipantCount)
