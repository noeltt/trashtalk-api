/* eslint-disable */
const Koa = require('koa')
// configEnv = config[ process.env.NODE_ENV || 'development' ],
const logger = require('koa-logger')
const bodyParser = require('koa-bodyparser')
const cors = require('@koa/cors')

const app = new Koa()
global.env = process.env.NODE_ENV ?? 'dev'

const router = require('./routes/routes')
const config = require('./config.json')[global.env]

const { contextBodyInHandler, contextBodyOutHandler } = require('./middleware/contextBodyHandler')
// okay if not used, it's how scheduler gets run
const scheduler = require('./utils/scheduler')

const { port } = config

const { tokenAuth } = require('./middleware/tokenAuth')

app.use(logger())
app.use(cors())
app.use(bodyParser())

app.use(async (context, next) => {
    context.set('Access-Control-Allow-Origin', '*')
    context.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    context.set('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS')
    context.set('Access-Control-Allow-Credentials', 'true')

    try {
        let { request } = context
        let { body = {}, header = {}, url, method } = request

        const { 'trashtalk-token': token } = header
        const { userId, userUid } = await tokenAuth(token, url, method)

        body.userId = userId
        body.userUid = userUid

        // contextBodyInHandler(body) // doesn't do anything yet

        await next()

        context.body = contextBodyOutHandler(context.body)
    } catch ({ message, status }) {
        console.error({ status, message })
        context.status = status || 503
        const errMessage = context.status === 503 ? 'Something went wrong' : message
        context.body = { message: errMessage }
    }
})

// must go here otherwise error handler does not work
app.use(router.routes.routes())

app.listen(port, () => {
    console.log(`Server listening on port: ${port} ==> ${config.whereAmI}`)
})
