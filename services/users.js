/* eslint-disable */

const { v4: uuidv4 } = require('uuid')
const { createToken } = require('../middleware/tokenAuth')
const bcrypt = require('bcryptjs')
const { sendRegistrationCode, verifyRegistrationCode } = require('../utils/codeUtils')

const userDAL = require('../DAL/usersDAL')
const postsDAL = require('../DAL/postsDAL')

const { paramInputError, paramInvalidError } = require('../utils/Errors')
const { uploadUserProfile, uploadUserBaseImage } = require('../utils/aws')
const { getMinDiffFromNow } = require('../utils/mainUtils')
const { isNil } = require('lodash')

// TODO: config salt rounds
const salt = bcrypt.genSaltSync(10)

/////////////////////////////
//    USER LOGIN/CREATION  //
/////////////////////////////
// POST - /login
exports.login = async (ctx) => {
    // change to email here and in DAL before launch
    const { email = '', password = '' } = ctx.request.body

    const { userUid, username, hashPassword, avatarUrl } = await userDAL.checkLogin(email)

    const validAuth = userUid ? bcrypt.compareSync(password, hashPassword) : false

    if (!validAuth) throw { status: 500, message: 'Invalid authentication' }

    let token = await createToken(userUid)

    ctx.body = {
        username,
        userUid,
        token,
        avatarUrl,
    }
}

// GET - /users/details
exports.getUserDetails = async (ctx) => {
    const { userId } = ctx.request.body

    const userDataRaw = await userDAL.getUserDetails(userId)

    if (userDataRaw === undefined) throw paramInvalidError

    ctx.body = userDataRaw
}

// GET - /users/profile?uid=
exports.getUserProfile = async (ctx) => {
    const { uid = '' } = ctx.request.query

    if (uid.length === 0) throw paramInputError

    const profileUserId = await userDAL.getUserIdFromUid(uid)

    if (profileUserId === 0) throw paramInvalidError

    const userDataRaw = await userDAL.getUserDetails(profileUserId)

    if (userDataRaw === undefined) throw paramInvalidError

    const counts = await userDAL.getUserProfileCounts(profileUserId)

    ctx.body = { ...userDataRaw, counts }
}

exports.getUserTopPosts = async (ctx) => {
    const { uid } = ctx.request.query
    const { userId } = ctx.request.body
    if (uid.length === 0) throw paramInputError

    const profileUserId = await userDAL.getUserIdFromUid(uid)

    if (profileUserId === 0) throw paramInvalidError

    const [postsData] = await userDAL.getUserTopPosts(userId, profileUserId)

    let topPosts = postsData.map((post) => {
        return {
            postId: post.id,
            title: post.title,
            body: post.body,
            imageUrl: post.image_url,
            authorUid: post.uid,
            commentCount: post.comment_count,
            likeCount: post.like_count,
            trashCount: post.trash_count,
            viewCount: post.view_count,
            postType: post.post_type,
            points: post.points,
            avatarUrl: post.avatarUrl,
            username: post.username,
            didUserLike: post.userLike === 1,
            didUserTrash: post.userTrash === 1,
            minDiff: getMinDiffFromNow(post.created_at),
        }
    })

    const pollPostIds = topPosts.filter((p) => p.postType === 1).map((p) => p.postId)

    const [pollPostsData] =
        pollPostIds.length > 0 ? await postsDAL.getPollPostsOptions(userId, pollPostIds) : []

    topPosts = topPosts.map((post) => {
        let newPost = { ...post }

        if (post.postType === 1) {
            const postPollData = pollPostsData.filter((ppd) => ppd.postId === post.postId)

            const userVoteOption = postPollData.find((ppd) => ppd.didUserVote === 1)

            const pollOptions = postPollData.map((ppd) => ({
                id: ppd.optionId,
                name: ppd.optionName,
                voteCount: ppd.voteCount,
            }))

            const totalVoteCount = postPollData.reduce(
                (prev, next) => prev + next.voteCount ?? 0,
                0
            )

            newPost.didUserVote = !isNil(userVoteOption)
            newPost.pollOptions = pollOptions
            newPost.totalVoteCount = totalVoteCount
            newPost.userVoteOption = isNil(userVoteOption) ? 0 : userVoteOption.optionId
        }

        return newPost
    })

    ctx.body = { topPosts }
}

// PUT - /users/details
exports.updateUserDetails = async (ctx) => {
    const { userId, username = '', bio = '' } = ctx.request.body

    if (username.length === 0 && bio.length === 0) throw paramInputError

    if (username.length > 0) {
        const usernameExists = await userDAL.usernameExists(username, userId)
        if (usernameExists) throw paramInvalidError

        await userDAL.updateUsername(username, userId)
    } else if (bio.length > 0) await userDAL.updateUserBio(bio, userId)

    ctx.body = {}
}

exports.createUser = async (ctx) => {
    const { email, username, password } = ctx.request.body

    const hashPassword = bcrypt.hashSync(password, salt)
    const newUid = uuidv4()

    const [ret] = await userDAL.createUser(email, username, hashPassword, newUid)

    const { insertId: userId } = ret

    await userDAL.giveBaseRaffleTickets(userId)

    sendRegistrationCode(userId, email)

    ctx.body = {}
}

exports.verifyRegistrationCode = async (ctx) => {
    const { code } = ctx.request.body

    const codeVerified = await verifyRegistrationCode(code)

    if (!codeVerified) throw paramInvalidError

    ctx.body = {}
}

// POST - /users/following
exports.followUser = async (ctx) => {
    const { userId, followingUid = '' } = ctx.request.body

    if (followingUid.length === 0) throw paramInputError

    const followingUserId = await userDAL.getUserIdFromUid(followingUid)

    if (followingUserId === 0) throw paramInvalidError

    // console.log({ userId, followingUserId })

    await userDAL.followUser(userId, followingUserId)

    ctx.body = {}
}

// DEL - /users/following
exports.unfollowUser = async (ctx) => {
    const { userId, followingUid = '' } = ctx.request.body

    if (followingUid.length === 0) throw paramInputError

    const followingUserId = await userDAL.getUserIdFromUid(followingUid)

    if (followingUserId === 0) throw paramInvalidError

    // console.log({ userId, followingUserId })

    await userDAL.unfollowUser(userId, followingUserId)

    ctx.body = {}
}

// GET /users?username=
exports.findUser = async (ctx) => {
    const { username = '' } = ctx.request.query
    const { userId } = ctx.request.body

    // if (username.length === 0) throw paramInputError
    let users = []

    if (username !== '') {
        const [usersRaw] = await userDAL.findUserByUsername(username, userId)

        users = usersRaw.map((x) => {
            return {
                ...x,
                following: x.followingRaw !== null,
            }
        })
    }

    ctx.body = { users }
}

// GET /users/following
exports.getFollowing = async (ctx) => {
    const { userId } = ctx.request.body

    const [users] = await userDAL.getUserFollowing(userId)

    ctx.body = { users }
}

// admin super power
exports.createInstaUser = async (ctx) => {
    ctx.body = {}
}

exports.updateUserProfilePicture = async (ctx) => {
    const { userId, userUid, imageBase64 } = ctx.request.body
    await uploadUserProfile(userId, userUid, imageBase64)

    ctx.body = {}
}

exports.addImage = async (ctx) => {
    const { userId, userUid, imageBase64 } = ctx.request.body
    const imageUrl = await uploadUserBaseImage(userId, imageBase64)
    ctx.body = {
        imageUrl,
    }
}

exports.deleteMe = async (ctx) => {
    const { userId } = ctx.request.body
    await userDAL.anonymizeUser(userId)
    ctx.body = {}
}

/////////////////////////////
//    INTERNAL FUNCTIONS   //
/////////////////////////////
