/* eslint-disable */
const statsDAL = require('../DAL/statsDAL')
const usersDAL = require('../DAL/usersDAL')
const seeder = require('../statsUtil/nba')

const { paramInvalidError, paramInputError } = require('../utils/Errors')
const { defaultTeamBigImage } = require('../utils/mainUtils')

const statusMap = {
    'quarter 4': 'Q4',
    'quarter 3': 'Q3',
    'quarter 2': 'Q2',
    'quarter 1': 'Q1',
    'half time': 'HT',
}

const quarterMap = {
    'quarter 4': 4,
    'quarter 3': 3,
    'quarter 2': 2,
    'quarter 1': 1,
    'half time': 2,
}

const convertGameData = (game) => ({
    gameId: game.id,
    startTime: game.start_time_utc,
    status: game.status === 'in play' ? 'live' : game.status,
    quarter: quarterMap[game.status] ?? 0,
    gameClock: game.clock,
    participantCount: game.chat_participant_count ?? 0,
    homeTeam: {
        teamId: game.home_team_id ?? 0,
        teamName: game.home_team_name,
        teamShortName: game.home_short_name ? game.home_short_name : '',
        teamRecord: `${game.home_win}-${game.home_loss}`,
        logoUrl: game.home_team_logo,
        score: game.home_score,
    },
    awayTeam: {
        teamId: game.away_team_id ?? 0,
        teamName: game.away_team_name,
        teamShortName: game.away_short_name ? game.away_short_name : '',
        teamRecord: `${game.away_win}-${game.away_loss}`,
        logoUrl: game.away_team_logo,
        score: game.away_score,
    },
})

const convertTeamData = (teamDataRaw) => ({
    teamId: teamDataRaw.id,
    fullName: teamDataRaw.full_name,
    logoUrl: teamDataRaw.logo_url,
    imageUrl: teamDataRaw.big_image_url || defaultTeamBigImage,
    followCount: teamDataRaw.follow_count,
    points: teamDataRaw.points,
    following: teamDataRaw.following === 1,
})

const convertTeamDetailData = (teamDataRaw) => ({
    ...convertTeamData(teamDataRaw),
    win: teamDataRaw.win,
    loss: teamDataRaw.loss,
    winPercentage: teamDataRaw.win_percentage,
    lossPercentage: teamDataRaw.loss_percentage,
    lastTenWin: teamDataRaw.last_ten_win,
    lastTenLoss: teamDataRaw.last_ten_loss,
    conference: teamDataRaw.conference,
    conferenceRank: teamDataRaw.conference_rank,
    conferenceWin: teamDataRaw.conference_win,
    conferenceLoss: teamDataRaw.conference_loss,
    homeWin: teamDataRaw.home_win,
    homeLoss: teamDataRaw.home_loss,
    awayWin: teamDataRaw.away_win,
    awayLoss: teamDataRaw.away_loss,
    winStreak: teamDataRaw.win_streak,
    gamesBehind: teamDataRaw.games_behind,
})

const convertPlayerData = (player) => ({
    id: player.id,
    firstName: player.first_name,
    lastName: player.last_name,
    team: {
        id: player.team_id,
        name: player.team_name,
        backgroundImageUrl: player.big_image_url || defaultTeamBigImage,
    },
    height: player.height.replace('"', "''"),
    position: player.position,
    positionShort: player.position_short,
    weight: player.weight,
    age: player.age,
    jerseyNumber: player.jersey_number,
    gamesPlayed: player.games_played,
    minutesPerGame: player.minutes_per_game,
    usagePercent: player.usage_percent,
    turnoverPercent: player.turnoverPercent,
    freethrowAttempts: player.freethrow_attempts,
    freethrowPercent: player.freethrow_percent,
    twopointAttempts: player.twopoint_attempts,
    twopointPercent: player.twopoint_percent,
    threepointAttempts: player.threepoint_attempts,
    threepointPercent: player.threepoint_percent,
    effectiveFieldGoalPercent: player.effective_field_goal_percent,
    trueShootingPercent: player.true_shooting_percent,
    pointsPerGame: player.points_per_game,
    reboundsPerGame: player.rebounds_per_game,
    totalReboundPercent: player.total_rebound_percent,
    assistsPerGame: player.assists_per_game,
    assistsPercent: player.assists_percent,
    stealsPerGame: player.steals_per_game,
    blocksPerGame: player.blocks_per_game,
    turnoverPerGame: player.turnover_per_game,
    versatilityIndex: player.versatility_index,
    offensiveRating: player.offensive_rating,
    defensiveRating: player.defensive_rating,
    headshotUrl: player.headshot_url,
})

// /////////////////////
//   TEAM ENDPOINTS   //
// /////////////////////

exports.test = async (ctx) => {
    await seeder.updateNBATeamDetails()
    ctx.body = {}
}

// GET - /teams
exports.getTeams = async (ctx) => {
    const { userId } = ctx.request.body
    const [teamData] = await statsDAL.getTeamList(userId)
    const teams = teamData.map((team) => convertTeamData(team))

    ctx.body = { teams }
}

// GET - /teams/:teamId
exports.getTeamData = async (ctx) => {
    const { userId } = ctx.request.body
    const { teamId = 0 } = ctx.request.params

    const teamDataRaw = await statsDAL.getTeamData(userId, teamId)
    const teamDetailData = convertTeamDetailData(teamDataRaw)

    ctx.body = {
        teamDetailData,
    }
}

// POST - /teams/:teamId/follow
exports.followTeam = async (ctx) => {
    const { userId } = ctx.request.body
    const { teamId = 0 } = ctx.request.params

    const nbaTeamId = parseInt(teamId)
    await statsDAL.followTeam(userId, nbaTeamId)

    ctx.body = {}
}

// PUT - /teams/:teamId/follow
exports.unfollowTeam = async (ctx) => {
    const { userId } = ctx.request.body
    const { teamId = 0 } = ctx.request.params

    const nbaTeamId = parseInt(teamId)
    await statsDAL.unfollowTeam(userId, nbaTeamId)

    ctx.body = {}
}

// GET - /teams/follow
exports.getFollowingTeams = async (ctx) => {
    const { userId } = ctx.request.body

    const [teamsData] = await statsDAL.getFollowingTeamData(userId)

    ctx.body = { teamsData }
}

exports.getProfileFollowingTeams = async (ctx) => {
    const { uid = '' } = ctx.request.query

    if (uid.length === 0) throw paramInputError

    const otherUserId = await usersDAL.getUserIdFromUid(uid)

    if (otherUserId === 0) throw paramInvalidError

    const [teamsData] = await statsDAL.getFollowingTeamData(otherUserId)

    ctx.body = { teamsData }
}

// /////////////////////
//   PLAYER ENDPOINTS   //
// /////////////////////

// GET = /teams/:teamId/players
exports.getTeamPlayers = async (ctx) => {
    const { teamId } = ctx.request.params

    const [teamPlayers] = await statsDAL.getTeamPlayers(teamId)

    const players = teamPlayers.map((player) => convertPlayerData(player))

    ctx.body = {
        players,
    }
}

exports.getPlayerData = async (ctx) => {
    const { playerId } = ctx.request.params

    const [playerData] = await statsDAL.getPlayerData(playerId)

    if (playerData.length === 0) throw paramInvalidError

    const player = convertPlayerData(playerData[0])

    ctx.body = {
        player,
    }
}

exports.findPlayer = async (ctx) => {
    const { name } = ctx.request.query

    let players = []

    if (name !== '') {
        const [playersRaw] = await statsDAL.findPlayerData(name)
        players = playersRaw
    } else {
        const [playersRaw] = await statsDAL.findDefaultPlayerData()
        players = playersRaw
    }

    ctx.body = { players }
}

// /////////////////////
//   GAME ENDPOINTS   //
// /////////////////////

// PUT - INTERNAL - from chat only
exports.updateGameParticipantCount = async (ctx) => {
    const { gameId, participantCount } = ctx.request.body

    await statsDAL.updateGameParticipantCount(gameId, participantCount)

    ctx.body = {}
}

exports.getUpcomingGames = async (ctx) => {
    const [gamesData] = await statsDAL.getUpcomingGames()

    const games = gamesData.map((game) => {
        // if (game.status !== 'not started') {
        //     // console.log({ game })
        // }
        return convertGameData(game)
    })

    ctx.body = { games }
}

exports.getPreviousGames = async (ctx) => {
    const [gamesData] = await statsDAL.getPreviousGames()
    const games = gamesData.map((game) => convertGameData(game))
    ctx.body = { games }
}

exports.getGameData = async (ctx) => {
    const { gameId } = ctx.request.params

    const gameData = await statsDAL.getGameData(gameId)
    if (gameData === undefined) throw paramInputError

    const game = convertGameData(gameData)

    ctx.body = { game }
}

exports.getGameLinks = async (ctx) => {
    const { gameId } = ctx.request.params
    const [gameLinks] = await statsDAL.getGameLinks(gameId)

    const links = gameLinks.map((x) => x.game_link ?? '')

    // console.log({ gameLinks })

    ctx.body = { links }
}
