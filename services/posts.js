/* eslint-disable */
const { isNil } = require('lodash')
const postsDAL = require('../DAL/postsDAL')
const usersDAL = require('../DAL/usersDAL')
const { paramInputError, paramInvalidError } = require('../utils/Errors')
const { defaultPostImageUrl, defaultAvatarUrl, getMinDiffFromNow } = require('../utils/mainUtils')
const { addPointsToPost, subtractPointsFromPost } = require('../utils/pointUtils')

const pointTiers = {
    tierOne: 1,
    tierTwo: 2,
    tierThree: 5,
}

// //////////////////////////////
//       INTERNAL FUNCTION    //
// //////////////////////////////
const doesPostExist = async (postId) => postsDAL.doesPostExist(postId)
const doesCommentExist = async (commentId) => postsDAL.doesCommentExist(commentId)

const paramCheck = async ({ postId, commentId, optionId, message, postTitle, postType }) => {
    // input checks
    // console.log({ postId, comment, option, message })
    if (parseInt(postId) === 0) throw paramInputError
    if (parseInt(commentId) === 0) throw paramInputError
    if (optionId === 0) throw paramInputError
    if (message && message.length === 0) throw paramInputError

    // validity checks
    if (postId > 0) {
        if (!(await doesPostExist(postId))) throw paramInvalidError
    } else if (commentId > 0) {
        if (!(await doesCommentExist(commentId))) throw paramInvalidError
    }

    return { postId, commentId, optionId, message }
}

const getPollPostVoteData = async (postId, userId, postData) => {
    let [postOptions] = await postsDAL.getPostOptions(postId)
    const [postVotes] = await postsDAL.getPostVotes(postId)
    const [didUserVoteRaw] = await postsDAL.didUserVote(postId, userId)

    let totalVoteCount = 0

    postVotes.forEach(({ voteCount, optionId }) => {
        postOptions[optionId - 1].voteCount = voteCount
        totalVoteCount += voteCount
    })

    const didUserVote = didUserVoteRaw.length > 0
    const userVoteOption = didUserVote ? didUserVoteRaw[0].option_id : 0
    const pollOptions = postOptions

    return { didUserVote, totalVoteCount, userVoteOption, pollOptions }
}

// //////////////////////////////
//       POSTS LEVEL           //
// //////////////////////////////
// POST - posts/
// type - 0 = discussion post
// type - 1 = poll post
// voteOption - string array
exports.createPost = async (ctx) => {
    let {
        postType = 0,
        postTitle = '',
        postBody = '',
        pollOptions,
        userId,
        postImageUrl = '',
        gameId = 0,
        teamId = 0,
        playerId = 0,
    } = ctx.request.body

    if (postTitle === '' || postType > 1) throw paramInputError
    if (postType === 0 && postTitle === '' && postBody === '') throw paramInputError
    if (postType === 1 && (pollOptions === undefined || pollOptions.length === 0))
        throw paramInputError

    postImageUrl = postImageUrl || ''

    await postsDAL.createPost({
        postType,
        pollOptions,
        postTitle,
        postBody,
        userId,
        postImageUrl,
        gameId,
        teamId,
        playerId,
    })

    ctx.body = {
        success: true,
    }
}

// POST - posts/:postId/likes
exports.likePost = async (ctx) => {
    const { userId } = ctx.request.body
    const { postId = 0 } = ctx.request.params

    await paramCheck({ postId })

    await postsDAL.likePost(userId, postId)
    addPointsToPost({ postId, points: pointTiers.tierOne })

    ctx.body = {}
}

// POST - posts/:postId/unlike
exports.unlikePost = async (ctx) => {
    const { userId } = ctx.request.body
    const { postId = 0 } = ctx.request.params

    await paramCheck({ postId })

    await postsDAL.unlikePost(userId, postId)
    subtractPointsFromPost({ postId, points: pointTiers.tierOne })

    ctx.body = {
        success: true,
    }
}

// POST - posts/:postId/untrash
exports.untrashPost = async (ctx) => {
    const { userId } = ctx.request.body
    const { postId = 0 } = ctx.request.params

    await paramCheck({ postId })

    await postsDAL.untrashPost(userId, postId)

    ctx.body = {
        success: true,
    }
}

// POST - posts/:postId/trash
exports.trashPost = async (ctx) => {
    const { userId } = ctx.request.body
    const { postId = 0 } = ctx.request.params

    await paramCheck({ postId })

    await postsDAL.trashPost(userId, postId)
    addPointsToPost({ postId, points: pointTiers.tierOne })

    ctx.body = {
        success: true,
    }
}

// GET - posts/:postId/
exports.getPostDetails = async (ctx) => {
    const { postId = 0 } = ctx.request.params
    const { userId } = ctx.request.body

    await paramCheck({ postId })

    let postDataRaw = await postsDAL.getPostDetails(postId, userId)

    if (postDataRaw === undefined) throw paramInvalidError

    let minDiff = getMinDiffFromNow(postDataRaw.created_at)

    // redundancy on defaults help cover and emergency db changes
    let postData = {
        postId: postDataRaw.id,
        type: postDataRaw.post_type,
        title: postDataRaw.title,
        body: postDataRaw.body || '',
        postImageUrl: postDataRaw.image_url,
        commentCount: postDataRaw.comment_count,
        viewCount: postDataRaw.view_count,
        likeCount: postDataRaw.like_count,
        trashCount: postDataRaw.trash_count,
        points: postDataRaw.points,
        pollOptions: [],
        minDiff,
    }

    if (postData.type === 1) {
        const { didUserVote, userVoteOption, totalVoteCount, pollOptions } =
            await getPollPostVoteData(postId, userId, postData)

        postData = { ...postData, didUserVote, userVoteOption, totalVoteCount, pollOptions }
    }

    let authorData = {
        username: postDataRaw.username,
        avatarUrl: postDataRaw.avatarUrl || defaultAvatarUrl,
        isAdmin: postDataRaw.isAdmin === 1,
        authorUid: postDataRaw.author_uid,
    }

    let userData = {
        userLike: postDataRaw.userLike != null,
        userTrash: postDataRaw.userTrash != null,
    }

    // need if user has liked or trashed post

    ctx.body = { postData, authorData, userData }
}

// GET - posts
// if none of the params are there, returns all
exports.getPosts = async (ctx) => {
    const { userId } = ctx.request.body
    const { gameId = '', teamId = '', playerId = '' } = ctx.request.query

    const [postsData] = await postsDAL.getPosts(gameId, teamId, playerId, userId)

    let posts = postsData.map((post) => {
        return {
            postId: post.id,
            title: post.title,
            body: post.body,
            imageUrl: post.image_url,
            authorUid: post.uid,
            commentCount: post.comment_count,
            likeCount: post.like_count,
            trashCount: post.trash_count,
            viewCount: post.view_count,
            postType: post.post_type,
            points: post.points,
            avatarUrl: post.avatarUrl,
            username: post.username,
            didUserLike: post.userLike === 1,
            didUserTrash: post.userTrash === 1,
            minDiff: getMinDiffFromNow(post.created_at),
        }
    })

    const pollPostIds = posts.filter((p) => p.postType === 1).map((p) => p.postId)

    const [pollPostsData] =
        pollPostIds.length > 0 ? await postsDAL.getPollPostsOptions(userId, pollPostIds) : []

    posts = posts.map((post) => {
        let newPost = { ...post }

        if (post.postType === 1) {
            const postPollData = pollPostsData.filter((ppd) => ppd.postId === post.postId)

            const userVoteOption = postPollData.find((ppd) => ppd.didUserVote === 1)

            const pollOptions = postPollData.map((ppd) => ({
                id: ppd.optionId,
                name: ppd.optionName,
                voteCount: ppd.voteCount,
            }))

            const totalVoteCount = postPollData.reduce(
                (prev, next) => prev + next.voteCount ?? 0,
                0
            )

            newPost.didUserVote = !isNil(userVoteOption)
            newPost.pollOptions = pollOptions
            newPost.totalVoteCount = totalVoteCount
            newPost.userVoteOption = isNil(userVoteOption) ? 0 : userVoteOption.optionId
        }

        return newPost
    })

    ctx.body = {
        posts,
    }
}

//TODO: combine post pulls somehow....?
exports.getHotPosts = async (ctx) => {
    const { userId } = ctx.request.body

    const [postsData] = await postsDAL.getHotPosts(userId)

    let posts = postsData.map((post) => {
        return {
            postId: post.id,
            title: post.title,
            body: post.body,
            imageUrl: post.image_url,
            authorUid: post.uid,
            commentCount: post.comment_count,
            likeCount: post.like_count,
            trashCount: post.trash_count,
            viewCount: post.view_count,
            postType: post.post_type,
            points: post.points,
            avatarUrl: post.avatarUrl,
            username: post.username,
            didUserLike: post.userLike === 1,
            didUserTrash: post.userTrash === 1,
            minDiff: getMinDiffFromNow(post.created_at),
        }
    })

    const pollPostIds = posts.filter((p) => p.postType === 1).map((p) => p.postId)

    const [pollPostsData] =
        pollPostIds.length > 0 ? await postsDAL.getPollPostsOptions(userId, pollPostIds) : []

    posts = posts.map((post) => {
        let newPost = { ...post }

        if (post.postType === 1) {
            const postPollData = pollPostsData.filter((ppd) => ppd.postId === post.postId)

            const userVoteOption = postPollData.find((ppd) => ppd.didUserVote === 1)

            const pollOptions = postPollData.map((ppd) => ({
                id: ppd.optionId,
                name: ppd.optionName,
                voteCount: ppd.voteCount,
            }))

            const totalVoteCount = postPollData.reduce(
                (prev, next) => prev + next.voteCount ?? 0,
                0
            )

            newPost.didUserVote = !isNil(userVoteOption)
            newPost.pollOptions = pollOptions
            newPost.totalVoteCount = totalVoteCount
            newPost.userVoteOption = isNil(userVoteOption) ? 0 : userVoteOption.optionId
        }

        return newPost
    })

    ctx.body = {
        posts,
    }
}

exports.reportPost = async (ctx) => {
    const { userId, postId } = ctx.request.body
    await postsDAL.reportPost(userId, postId)
    ctx.body = {}
}

exports.hidePost = async (ctx) => {
    const { userId, postId } = ctx.request.body
    if (!postId) throw paramInputError
    console.log({ userId, postId })
    await postsDAL.hidePost(userId, postId)
    ctx.body = {}
}

exports.blockUser = async (ctx) => {
    const { userId, hideUserUid } = ctx.request.body
    const hideUserId = await usersDAL.getUserIdFromUid(hideUserUid)

    if (hideUserId === 0) throw paramInvalidError

    await postsDAL.hideUser(userId, hideUserId)

    ctx.body = {}
}

// //////////////////////////////
//       POLL POSTS            //
// //////////////////////////////
// POST - posts/:postId/votes
// no unvoting
exports.voteForPoll = async (ctx) => {
    const { userId, optionId = 0 } = ctx.request.body
    const { postId = 0 } = ctx.request.params

    await paramCheck({ postId, optionId })

    // not sure why this requires parseInt will look into it later
    await postsDAL.voteForPoll(postId, optionId, userId)
    addPointsToPost({ postId, points: pointTiers.tierTwo })

    ctx.body = {}
}

// GET - posts/:postId/votes
exports.getPostPollResults = async (ctx) => {
    const { postId = 0 } = ctx.request.params

    await paramCheck({ postId })

    const [postVotesData] = await postsDAL.getPostVotes(postId)

    let votes = {}

    for (let i = 0; i < postVotesData.length; i++) {
        const postVote = postVotesData[i]
        if (i === 0) {
            votes.postId = postVote.post_id
        }

        if (!votes[postVote.option_id]) {
            votes[postVote.option_id] = {}
            votes[postVote.option_id].optionName = postVote.option_name
            votes[postVote.option_id].userIds = []
        }

        votes[postVote.option_id].userIds.push(postVote.user_id)
    }

    ctx.body = { votes }
}

// //////////////////////////////
//       COMMENTS LEVEL       //
// //////////////////////////////

// POST - posts/:postId/comments
exports.createComment = async (ctx) => {
    let { postId = 0 } = ctx.request.params
    let { userId, message = '' } = ctx.request.body

    message = message.trim()

    await paramCheck({ postId, message })

    await postsDAL.createPostComment(postId, { userId, message })
    addPointsToPost({ postId, points: pointTiers.tierThree })

    ctx.body = {}
}

// POST - comments/:commentId/like
exports.likePostComment = async (ctx) => {
    let { commentId = 0 } = ctx.request.params
    let { userId } = ctx.request.body

    await paramCheck({ commentId })
    await postsDAL.likeComment(userId, commentId)

    ctx.body = {}
}

// PUT - comments/:commentId/unlike
exports.unlikePostComment = async (ctx) => {
    const { commentId = 0 } = ctx.request.params
    const { userId } = ctx.request.body

    await paramCheck({ commentId })
    await postsDAL.unlikeComment(userId, commentId)

    ctx.body = {}
}

// POST - comments/:commentId/trash
exports.trashPostComment = async (ctx) => {
    let { commentId = 0 } = ctx.request.params
    let { userId } = ctx.request.body

    await paramCheck({ commentId })
    await postsDAL.trashComment(userId, commentId)

    ctx.body = {}
}

// PUT - comments/:commentId/untrash
exports.untrashPostComment = async (ctx) => {
    let { commentId = 0 } = ctx.request.params
    let { userId } = ctx.request.body

    await paramCheck({ commentId })
    await postsDAL.untrashComment(userId, commentId)

    ctx.body = {}
}

// GET - posts/:postId/comments
// limit 30
// paginated eventually
exports.getPostComments = async (ctx) => {
    const { postId = 0 } = ctx.request.params
    const { userId } = ctx.request.body

    await paramCheck({ postId })

    let [comments] = await postsDAL.getPostComments(userId, postId)

    comments = comments.map((comment) => {
        let newComment = {
            commentData: {
                id: comment.id,
                postId: comment.post_id,
                message: comment.message,
                likeCount: comment.like_count,
                trashCount: comment.trash_count,
                // replyCount: reply_count,
                points: comment.points,
                minDiff: getMinDiffFromNow(comment.created_at),
            },
            authorData: {
                username: comment.username,
                avatarUrl: comment.avatarUrl || defaultAvatarUrl,
            },
            userData: {
                userLike: comment.userLike != null,
                userTrash: comment.userTrash != null,
            },
        }

        return newComment
    })

    ctx.body = { comments }
}

// //////////////////////////////
//   POST REPLIES LEVEL       //
// //////////////////////////////

// get comment replies T.T
