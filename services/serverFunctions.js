/* eslint-disable */
// const { createToken } = require('../middleware/tokenAuth')
const { baseError, authError } = require('../utils/Errors')
const config = require('../config.json')[global.env]
const scraper = require('../utils/scraper')

exports.health = async (ctx) => {
    // await createToken()

    // await scraper.updatePlayerStats()

    // throw baseError(500, 'something')
    ctx.body = {
        text: `server is okay! ${config.whereAmI}`,
    }
}
