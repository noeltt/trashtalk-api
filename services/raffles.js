/*eslint-disable*/
const rafflesDAL = require('../DAL/rafflesDAL')
const { paramInvalidError } = require('../utils/Errors')

// get currentRaffles
exports.getCurrentRaffles = async (ctx) => {
    const { userId } = ctx.request.body

    const { ticketCount } = await rafflesDAL.getUserAvailableTicketCount(userId)
    const [rafflesRaw] = await rafflesDAL.getCurrentRaffles(userId)

    const raffles = rafflesRaw.map(({ id, name, image_url, closes_at, submitted }) => ({
        raffleId: id,
        name,
        image_url,
        closes: closes_at,
        entered: submitted === 1,
    }))

    // user submitted

    ctx.body = {
        availableTickets: ticketCount,
        raffles,
    }
}

exports.submitRaffleTicket = async (ctx) => {
    // current 1 tix per raffle check
    const { userId, raffleId } = ctx.request.body

    const userInRaffle = await rafflesDAL.getUserInRaffle(userId, raffleId)

    // queryFirstRow
    const userAvailableTicket = (await rafflesDAL.getUserAvailableRaffleTicket(userId)) ?? null

    // could possible throw error here but no point really
    if (!userInRaffle && userAvailableTicket) {
        const { id: ticketId } = userAvailableTicket
        await rafflesDAL.addUserRaffleTicket(userId, raffleId, ticketId)
    } else {
        throw paramInvalidError
    }

    ctx.body = {}
}
